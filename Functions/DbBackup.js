const cron = require('node-cron');
const schedule = require('node-schedule');
const featureFactory = require('../Schema/UserFeatureSettings')
const plan = require('../Schema/Plan')
const workspace = require('../Schema/Workspace')
const manageCacheAccounts = require("./ManageCacheAccounts")

const deactivateWorkspaces = async (id) => {
    try {
        const workspaceData = await workspace.find({ user_id: id })
        workspaceData.map(async (data) => {
            if (data.default_personal === false) {
                data.active = false
                await data.save()
            }
        })
    } catch (error) {
        console.log(error)
    }
}

const sheduleBackup = async () => {
    addExpireToPlans()
    addYearlyPlanMothlyCredits()
    cron.schedule('0 0 * * *', function () {
        addYearlyPlanMothlyCredits()
        addExpireToPlans()
    })
}

const addYearlyPlanMothlyCredits = async () => {
    const start = new Date();
    start.setHours(0, 0, 0, 0);

    const end = new Date();
    end.setHours(23, 59, 59, 999);

    // @desc if any account remain to add credits then the credits will get add here
    let ifRemainToAddCredit = await featureFactory.find({ reCreditDate: { $lt: start }, planTakenForTime: 'yearly' })

    if (ifRemainToAddCredit !== 0) {
        const date2 = new Date()
        const creditNewDate = date2.setDate(date2.getDate() + 30)

        for await (const userCurrentPlan of ifRemainToAddCredit) {
            if (userCurrentPlan.deal === "app sumo") {
                let creditToAddFromPlan = await plan.findOne({ _id: userCurrentPlan.planId })
                if (userCurrentPlan.userPlans.includes("captionator")) {
                    userCurrentPlan.aiCredites = "50000"
                    userCurrentPlan.hashtagSearch = "0"
                    userCurrentPlan.hashtagSearchInstagram = "5"
                    userCurrentPlan.reCreditDate = creditNewDate
                    await userCurrentPlan.save()
                } else {
                    userCurrentPlan.aiCredites = "5000"
                    userCurrentPlan.hashtagSearch = creditToAddFromPlan.hashtagSearch
                    userCurrentPlan.hashtagSearchInstagram = creditToAddFromPlan.hashtagSearchInstagram
                    userCurrentPlan.reCreditDate = creditNewDate
                    await userCurrentPlan.save()
                }
            } else {
                let creditToAddFromPlan = await plan.findOne({ _id: userCurrentPlan.planId })
                if (userCurrentPlan.planId.toString() === "6247085137b553074fb6b25a") {
                    userCurrentPlan.aiCredites = creditToAddFromPlan.aiCredites
                } else {
                    userCurrentPlan.aiCredites = creditToAddFromPlan.aiCredites * userCurrentPlan.users
                }
                userCurrentPlan.hashtagSearch = creditToAddFromPlan.hashtagSearch
                userCurrentPlan.hashtagSearchInstagram = creditToAddFromPlan.hashtagSearchInstagram
                userCurrentPlan.reCreditDate = creditNewDate
                await userCurrentPlan.save()
            }
        }
    }

    // @desc get the accounts which credit going to over and add credit
    let data = await featureFactory.find({ reCreditDate: { $gte: start, $lt: end }, planTakenForTime: 'yearly' })
    data.forEach(userCurrentPlan => {
        let endDatePlan = new Date(userCurrentPlan.endDate)
        schedule.scheduleJob(endDatePlan, async () => {
            let planData = await featureFactory.findById(userCurrentPlan._id)

            const userPlanExpirationDate = new Date(planData.reCreditDate)
            const currentDate = new Date()
            if (currentDate < userPlanExpirationDate) return

            const date2 = new Date()
            const creditNewDate = date2.setDate(date2.getDate() + 30)

            let creditToAddFromPlan = await plan.findOne({ _id: planData.planId })
            planData.aiCredites = creditToAddFromPlan.aiCredites * userCurrentPlan.users
            planData.hashtagSearch = creditToAddFromPlan.hashtagSearch
            planData.hashtagSearchInstagram = creditToAddFromPlan.hashtagSearchInstagram
            planData.reCreditDate = creditNewDate
            await planData.save()
        })
    })
}

const addExpireToPlans = async () => {
    const start = new Date();
    start.setHours(0, 0, 0, 0);

    const end = new Date();
    end.setHours(23, 59, 59, 999);

    let freePlan = await plan.findOne({ planName: 'free' })

    const date = new Date()
    const endDate = date.setDate(date.getDate() + 30)

    // find if any plan remain to expire
    let ifRemainToExpireData = await featureFactory.find({ endDate: { $lt: start } })

    if (ifRemainToExpireData.length !== 0) {
        for await (const userCurrentPlan of ifRemainToExpireData) {
            if (userCurrentPlan.planId === freePlan._id) {
                userCurrentPlan.aiCredites = freePlan.aiCredites
                userCurrentPlan.hashtagSearch = freePlan.hashtagSearch
                userCurrentPlan.hashtagSearchInstagram = freePlan.hashtagSearchInstagram
                userCurrentPlan.startDate = new Date()
                userCurrentPlan.endDate = endDate
                await userCurrentPlan.save()
            } else {
                userCurrentPlan.planId = freePlan._id
                userCurrentPlan.workspace = freePlan.workspace
                userCurrentPlan.users = freePlan.users
                userCurrentPlan.socialAccounts = freePlan.socialAccounts
                userCurrentPlan.schedulePost = freePlan.schedulePost
                userCurrentPlan.aiCredites = freePlan.aiCredites
                userCurrentPlan.hashtagSearch = freePlan.hashtagSearch
                userCurrentPlan.hashtagSearchInstagram = freePlan.hashtagSearchInstagram
                userCurrentPlan.multipleTemplateResolution = freePlan.multipleTemplateResolution
                userCurrentPlan.transperentBackground = freePlan.transperentBackground
                userCurrentPlan.startDate = new Date()
                userCurrentPlan.endDate = endDate
                await userCurrentPlan.save()
                manageCacheAccounts(userCurrentPlan.userId)
                deactivateWorkspaces(userCurrentPlan.userId)
            }
        }
    }

    let data = await featureFactory.find({ endDate: { $gte: start, $lt: end } })

    data.forEach(userCurrentPlan => {
        let endDatePlan = new Date(userCurrentPlan.endDate)
        schedule.scheduleJob(endDatePlan, async () => {
            let planData = await featureFactory.findById(userCurrentPlan._id)

            const userPlanExpirationDate = new Date(planData.endDate)
            const currentDate = new Date()
            if (currentDate < userPlanExpirationDate) return

            const date2 = new Date()
            const endDate2 = date2.setDate(date.getDate() + 30)

            if (userCurrentPlan.planId === freePlan._id) {
                userCurrentPlan.aiCredites = freePlan.aiCredites
                userCurrentPlan.hashtagSearch = freePlan.hashtagSearch
                userCurrentPlan.hashtagSearchInstagram = freePlan.hashtagSearchInstagram
                userCurrentPlan.startDate = new Date()
                userCurrentPlan.endDate = endDate2
                await userCurrentPlan.save()
            } else {
                let freePlan = await plan.findOne({ planName: 'free' })
                userCurrentPlan.planId = freePlan._id
                userCurrentPlan.workspace = freePlan.workspace
                userCurrentPlan.users = freePlan.users
                userCurrentPlan.socialAccounts = freePlan.socialAccounts
                userCurrentPlan.schedulePost = freePlan.schedulePost
                userCurrentPlan.aiCredites = freePlan.aiCredites
                userCurrentPlan.hashtagSearch = freePlan.hashtagSearch
                userCurrentPlan.hashtagSearchInstagram = freePlan.hashtagSearchInstagram
                userCurrentPlan.multipleTemplateResolution = freePlan.multipleTemplateResolution
                userCurrentPlan.transperentBackground = freePlan.transperentBackground
                userCurrentPlan.startDate = new Date()
                userCurrentPlan.endDate = endDate2
                await userCurrentPlan.save()
                manageCacheAccounts(userCurrentPlan.userId)
                deactivateWorkspaces(userCurrentPlan.userId)
            }
        })
    })
}


module.exports = {
    sheduleBackup
}


