const cron = require('node-cron');
const UserFeatureSettings = require('../Schema/UserFeatureSettings')
const { downgradeToFreePlan, updateCreditsForPlansLongerThanOneMonth } = require('./featureSettings');

// Helper to calculate dates
const calculateDates = () => {
    const todayStart = new Date();
    todayStart.setHours(0, 0, 0, 0);

    const extendedEndDate = new Date();
    extendedEndDate.setDate(extendedEndDate.getDate() + 10);

    return { todayStart, extendedEndDate };
};

const handleExpiredPlans = async () => {
    try {
        const { todayStart } = calculateDates();

        // Fetch users with expired plans
        const expiredPlans = await UserFeatureSettings.find(
            { endDate: { $lt: todayStart } },
            { userId: 1 } // Fetch only userId for efficiency
        );

        if (expiredPlans.length === 0) {
            console.log("No plans expires today");
            return;
        }

        const downgradePromises = expiredPlans.map(plan => downgradeToFreePlan(plan.userId))
        await Promise.all(downgradePromises)

        console.log(`${expiredPlans.length} plans downgraded to the free plan.`);
    } catch (error) {
        console.error("Error processing expired plans:", error);
    }
}

const handlePlanRecredits = async () => {
    try {
        const { todayStart, extendedEndDate } = calculateDates();

        // Fetch users with expired plans who are still eligible for recredit
        const reCreditPlans = await UserFeatureSettings.find(
            {
                reCreditDate: { $lt: todayStart }, // Recredit date is before today
                endDate: { $gte: extendedEndDate } // End date is at least 10 days from now
            },
            { userId: 1, planId: 1 } // Fetch only necessary fields for efficiency
        );

        if (!reCreditPlans.length) {
            console.log("No plans require recrediting.");
            return;
        }

        // Update credits for each plan concurrently
        const recreditPromises = reCreditPlans.map(plan =>
            updateCreditsForPlansLongerThanOneMonth(plan.userId, plan.planId)
        );

        await Promise.all(recreditPromises);

        console.log(reCreditPlans.length, "plans recredited successfully.");
    } catch (error) {
        console.error("Error processing recredit plans:", error);
    }
};


const scheduleDailyPlanExpiryCheckAndRecredit = () => {
    handleExpiredPlans();
    handlePlanRecredits();

    cron.schedule('0 0 * * *', () => {
        handleExpiredPlans();
        handlePlanRecredits();
    });
}

module.exports = {
    scheduleDailyPlanExpiryCheckAndRecredit
}