const Plan = require("../Schema/Plan")
const UserFeatureSettings = require("../Schema/UserFeatureSettings")
const Workspace = require("../Schema/Workspace")
const manageCacheAccounts = require("./ManageCacheAccounts")

const deactivateWorkspaces = async (userId) => {
    try {
        // Fetch all non-personal workspaces for the given user
        const workspaces = await Workspace.find({ user_id: userId, default_personal: false });

        if (workspaces.length === 0) return

        // Update the 'active' status of all eligible workspaces
        const updatePromises = workspaces.map((data) => {
            data.active = false;
            return data.save();
        });

        await Promise.all(updatePromises);
    } catch (error) {
        console.error(`Error deactivating workspaces for user ID: ${userId}`, error);
    }
};

const activateWorkspaces = async (userId) => {
    try {
        // Fetch all non-personal workspaces for the given user
        const workspaces = await Workspace.find({ user_id: userId, default_personal: false });

        if (workspaces.length === 0) return

        // Update the 'active' status of all eligible workspaces
        const updatePromises = workspaces.map((data) => {
            data.active = true;
            return data.save();
        });

        await Promise.all(updatePromises);
    } catch (error) {
        console.error(`Error activating workspaces for user ID: ${userId}`, error);
    }
}


const addDuration = (duration, date = new Date()) => {
    const currentDate = new Date(date);

    if (duration === "1 month" || duration === "month") {
        currentDate.setMonth(currentDate.getMonth() + 1);
    } else if (duration === "3 month") {
        currentDate.setMonth(currentDate.getMonth() + 3);
    } else if (duration === "6 month") {
        currentDate.setMonth(currentDate.getMonth() + 6);
    } else if (duration === "year") {
        currentDate.setFullYear(currentDate.getFullYear() + 1); // Adds one year accurately
    }

    return currentDate.getTime(); // Returns timestamp
};

// Map "unlimited" values to null or use defaults from planInfo
const mapUnlimitedValue = (value) => (value === 'unlimited' ? null : value);

const updatePlanInfo = async (userId, planId, duration = "month", update = false) => {
    try {
        const featureSettings = await UserFeatureSettings.findOne({ userId: userId })
        const planInfo = await Plan.findOne({ _id: planId });
        const startDate = new Date() // Calculate the start, recredit and end dates

        if (!update) {
            featureSettings.planId = planInfo._id
            featureSettings.workspace = mapUnlimitedValue(planInfo.workspace)
            featureSettings.brandKit = mapUnlimitedValue(planInfo.brandKit)
            featureSettings.users = mapUnlimitedValue(planInfo.users)
            featureSettings.socialAccounts = mapUnlimitedValue(planInfo.socialAccounts)
            featureSettings.schedulePost = mapUnlimitedValue(planInfo.schedulePost)
            featureSettings.storage = mapUnlimitedValue(planInfo.storage)
        }

        featureSettings.aiCredits = mapUnlimitedValue(planInfo.aiCredites)
        featureSettings.aiImageCredits = mapUnlimitedValue(planInfo.aiImageCredits)

        if (!update) {
            featureSettings.startDate = startDate
            featureSettings.endDate = addDuration(duration, startDate)
        }

        featureSettings.reCreditDate = planInfo.planName === "free" ? undefined : addDuration("month", startDate)
        const updatedInfo = await featureSettings.save()
        return updatedInfo
    } catch (error) {
        console.error("Error updating plan info:", error)
        return null
    }
}

// Update credits for plans longer than one month
const updateCreditsForPlansLongerThanOneMonth = async (userId, planId) => {
    try {
        const update = true
        const data = await updatePlanInfo(userId, planId, "month", update)
        return data
    } catch (error) {
        return null
    }
}

// Downgrade to free plan
const downgradeToFreePlan = async (userId) => {
    try {
        const freePlan = await Plan.findOne({ planName: 'free' })
        const planId = freePlan._id
        const duration = "month"
        const data = await updatePlanInfo(userId, planId, duration)
        deactivateWorkspaces(userId)
        manageCacheAccounts(userId)
        return data
    } catch (error) {
        return null
    }
}

// Upgrade to a paid plan
const upgradeToPaidPlan = async (userId, planId, duration) => {
    try {
        const data = await updatePlanInfo(userId, planId, duration)
        manageCacheAccounts(userId)
        activateWorkspaces(userId)
        return data
    } catch (error) {
        return null
    }
}

// Create default user feature settings and assign them to the free plan
const createUserFeatureSettingsDefault = async (userId) => {
    try {
        // Fetch the free plan details
        const planInfo = await Plan.findOne({ planName: 'free' });
        if (!planInfo) {
            throw new Error('Free plan not found');
        }

        // Calculate the start and end dates
        const startDate = new Date();
        const endDate = addDuration("month", startDate);

        const data = await UserFeatureSettings.create({
            userId: userId,
            planId: planInfo._id,
            workspace: mapUnlimitedValue(planInfo.workspace),
            brandKit: mapUnlimitedValue(planInfo.brandKit),
            users: mapUnlimitedValue(planInfo.users),
            socialAccounts: mapUnlimitedValue(planInfo.socialAccounts),
            schedulePost: mapUnlimitedValue(planInfo.schedulePost),
            aiCredits: mapUnlimitedValue(planInfo.aiCredites),
            aiImageCredits: mapUnlimitedValue(planInfo.aiImageCredits),
            storage: mapUnlimitedValue(planInfo.storage),
            startDate: startDate,    // Set the start date to now
            endDate: endDate,        // Set the end date to 30 days from the start date
        })

        return data
    } catch (error) {
        return null
    }
}


module.exports = {
    upgradeToPaidPlan,
    downgradeToFreePlan,
    createUserFeatureSettingsDefault,
    updateCreditsForPlansLongerThanOneMonth
}