const nodemailer = require('nodemailer');
const sesTransport = require('nodemailer-ses-transport');
const user = require('../Schema/User')
const schedule = require('node-schedule');

const transporter = nodemailer.createTransport(
    sesTransport({
        accessKeyId: 'AKIAQ5RO3HORQD7AMD4E',
        secretAccessKey: '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',
        region: 'ap-south-1',
        rateLimit: 5,
    })
);


const sendMalavEmailAfterUserSinupWithin2Hours = async (email, name) => {
    const mailOptions = {
        from: 'Malav from Creatosaurus <account@creatosaurus.io>',
        to: email,
        subject: '10x your storytelling in half the time with Creatosaurus 🤘🏻',
        text: `Hi ${name},\nMalav here from Creatosaurus.\n\nI wanted to thank you for trying out Creatosaurus!\n\nI’m reaching out because there are a lot of features left to discover that are incredibly relevant to your role & industry like hashtag analysis, AI content writer, graphic designer and more.\n\nWe’ve seen that a quick tour of the product catered to you can help you maximize your impact for better conversions. It will help you 10x your storytelling in half the time.\n\nDo you have a moment to chat this week or early next week?\n\nYou can book a 30 mins video call slot here(https://cal.com/malavwarke/getting-started-with-creatosaurus). or You can also reply to the same email.\n\nLooking forward to having an exciting conversation with you.\n\nCheers,\nMalav Warke\nCo-Founder & CEO\nCreatosaurus\nhttps://www.linkedin.com/in/malavwarke/`
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        }
    })
}

const sendMayurEmailAfterUserSinupWithin3Days = async (email, name) => {
    const mailOptions = {
        from: 'Mayur from Creatosaurus <account@creatosaurus.io>',
        to: email,
        subject: "Don't blend in; Tell your your story ✨",
        text: `Hi ${name},\nMayur here from Creatosaurus.\n\nI wanted to thank you for trying out Creatosaurus!\n\nI’m reaching out because we want to know how comfortable you are with the product. What improvements you think we need to make so that you can improve your storytelling by 10x in half the time.\n\nGetting feedback from our community has helped us improve the product. In a way it has helped countless others create stories faster and collaborate seamlessly with their teams.\n\nYou can book a 30 mins video call slot here(https://calendly.com/gaikwadmayur7474/creatosaurus). or You can also reply to the same email.\n\nIt would be great to hear your valuable feedback.\n\nThanks,\nMayur Gaikwad\nCo-Founder & CTO\nCreatosaurus\nhttps://www.linkedin.com/in/mayur-gaikwad-817994184/`
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        }
    })
}

const sendMalavEmailAfterUserSinupWithin7Days = async (email, name) => {
    const mailOptions = {
        from: 'Malav from Creatosaurus <account@creatosaurus.io>',
        to: email,
        subject: "Don't blend in; Tell your your story ✨",
        text: `Hi ${name},Malav here from Creatosaurus.\n\nWanted to thank you for using Creatosaurus!\n\nOur goal is to be sincere in our efforts. Each day we are learning, improving and getting better.\n\nWe would like to know your exepreince from the last week using Creatosaurus. Feedbacks are most welcomed.\n\nIf you need any help or even a product walk through, feel free to reach me out.\n\nYou can also book a 30 mins video call slot here(https://cal.com/malavwarke/getting-started-with-creatosaurus). or You can also reply to the same email.\n\nLooking forward to having an exciting conversation with you.\n\nSincerely,\nMalav Warke\nCo-Founder & CEO\n`
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        }
    })
}

const capitalizeFirstLetter = (string) => {
    return string?.charAt(0).toUpperCase() + string?.slice(1);
}

const check2HourMailSent = async () => {
    try {
        const data = await user.find({ malavEmailSent: false, emailVerified: true })

        let currentDate = new Date()
        data.map(async userInfo => {
            let userSingUpDate = new Date(userInfo.createdAt)
            let hourDiff = parseInt(Math.abs(currentDate.getTime() - userSingUpDate.getTime()) / 3600000)
            if (hourDiff > 2) {
                let name = capitalizeFirstLetter(userInfo.firstName)
                if (isNaN(name) && typeof name !== 'string') {
                    name = ""
                }
                sendMalavEmailAfterUserSinupWithin2Hours(userInfo.email, name)
                userInfo.malavEmailSent = true
                await userInfo.save()
            }
        })
    } catch (error) {
        console.log(error)
    }
}


const checkDay3MailSent = async () => {
    try {
        const data = await user.find({ mayurEmailSent: false, emailVerified: true })

        let currentDate = new Date()
        data.map(async userInfo => {
            let userSingUpDate = new Date(userInfo.createdAt)
            let daysDiff = Math.abs(parseInt((userSingUpDate - currentDate) / (1000 * 60 * 60 * 24), 10))
            if (daysDiff > 1) {
                let name = capitalizeFirstLetter(userInfo.firstName)
                if (isNaN(name) && typeof name !== 'string') {
                    name = ""
                }
                sendMayurEmailAfterUserSinupWithin3Days(userInfo.email, name)
                userInfo.mayurEmailSent = true
                await userInfo.save()
            }
        })
    } catch (error) {
        console.log(error)
    }
}


const checkDay7MailSent = async () => {
    try {
        const data = await user.find({ malav2EmailSent: false, emailVerified: true })

        let currentDate = new Date()
        data.map(async userInfo => {
            let userSingUpDate = new Date(userInfo.createdAt)
            let daysDiff = Math.abs(parseInt((userSingUpDate - currentDate) / (1000 * 60 * 60 * 24), 10))
            if (daysDiff > 6) {
                let name = capitalizeFirstLetter(userInfo.firstName)
                if (isNaN(name) && typeof name !== 'string') {
                    name = ""
                }
                sendMalavEmailAfterUserSinupWithin7Days(userInfo.email, name)
                userInfo.malav2EmailSent = true
                await userInfo.save()
            }
        })
    } catch (error) {
        console.log(error)
    }
}

const checkEmailSentOrNot = async () => {
    check2HourMailSent()
    checkDay3MailSent()
    checkDay7MailSent()

    schedule.scheduleJob('*/30 * * * *', async () => {
        check2HourMailSent()
    })

    schedule.scheduleJob('0 0 */12 * *', async () => {
        checkDay3MailSent()
        checkDay7MailSent()
    })
}


module.exports = {
    checkEmailSentOrNot
}