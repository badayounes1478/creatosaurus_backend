const express = require('express')
const router = express.Router()
const Afilliate = require('../Schema/Afilliate')
const UserFeatureFactory = require("../Schema/UserFeatureFactory")
const LinkClicked = require("../Schema/LinkClicked")
const Transaction = null

router.get("/user/:id", async (req, res) => {
    try {
        // Find afilliate data based on user ID, excluding unnecessary fields & Extract afilliate user IDs
        const afilliateData = await Afilliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 });
        const afilliateIds = afilliateData.afilliateUserAdded.map(user => user._id);

        // Find user plan details based on extracted IDs, populate necessary fields
        const userPlanDetails = await UserFeatureFactory.find({ userId: { $in: afilliateIds } })
            .populate({ path: 'planId', select: 'planName -_id' })
            .populate({ path: 'userId', select: 'email lastName firstName createdAt' });

        // Filter and format user details, excluding users without email
        const filteredUserDetails = userPlanDetails
            .filter(data => data.userId && data.userId.email)
            .map(data => ({
                userId: data.userId._id,
                planName: data.planId?.planName,
                email: data.userId.email,
                firstName: data.userId.firstName,
                lastName: data.userId.lastName,
                createdAt: data.userId.createdAt
            }));

        // Find link click data based on user ID, excluding unnecessary fields
        const linkClickedData = await LinkClicked.findOne({ userId: req.params.id }, { userId: 0, _id: 0, __v: 0 });

        const salesCount = filteredUserDetails.filter(data => data.planName !== "free")

        let revenue = 0
        const promises = filteredUserDetails.map(async data => {
            const transactionDetails = await Transaction.find({ userId: data.userId }).sort({ _id: -1 });
            if (transactionDetails.length !== 0) {
                if (transactionDetails[0].paymentGateway === "stripe") {
                    if (transactionDetails[0].paymentInfo.currency === "inr") {
                        let inrPrice = transactionDetails[0].paymentInfo.amount_total / 100

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrPrice * percentage) / 100
                        revenue += percentageAmount * usdExchangeRate
                    } else {
                        let usdPrice = transactionDetails[0].paymentInfo.amount_total / 100
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        revenue += percentageAmount
                    }
                } else if (transactionDetails[0].paymentGateway === "ccavenue") {
                    if (transactionDetails[0].paymentInfo.currency.trim() === "INR") {
                        let inrPrice = parseInt(transactionDetails[0].paymentInfo.amount)

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrPrice * percentage) / 100
                        revenue += percentageAmount * usdExchangeRate
                    } else {
                        let usdPrice = parseInt(transactionDetails[0].paymentInfo.amount)
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        revenue += percentageAmount
                    }
                } else {
                    // we are not taking inr price because stripe does not support that
                    let usdPrice = parseInt(transactionDetails[0].paymentInfo.transactions[0].amount.total)
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    revenue += percentageAmount
                }
            }

            return data
        })

        await Promise.all(promises);

        let cutRevenue = await getSubAffiliateRevenue(req.params.id)

        res.status(200).json({
            linkClicksCount: linkClickedData?.days.length || 0,
            usersDetails: filteredUserDetails,
            salesCount: salesCount.length,
            revenue: revenue + cutRevenue
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.get("/customer/insight/:id/:days", async (req, res) => {
    try {
        const currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - parseInt(req.params.days));
        currentDate.setHours(0, 0, 0, 0);

        // Find afilliate data based on user ID, excluding unnecessary fields & Extract afilliate user IDs
        const afilliateData = await Afilliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 }).populate({
            path: "afilliateUserAdded",
            match: {
                createdAt: { $gte: currentDate }
            },
            select: "_id"
        })

        const afilliateIds = afilliateData.afilliateUserAdded.map(user => user._id);

        // Find user plan details based on extracted IDs, populate necessary fields
        const userPlanDetails = await UserFeatureFactory.find({ userId: { $in: afilliateIds } })
            .populate({ path: 'planId', select: 'planName -_id' })
            .populate({ path: 'userId', select: 'email lastName firstName createdAt' });

        // Filter and format user details, excluding users without email
        let filteredUserDetails = userPlanDetails
            .filter(data => data.userId && data.userId.email)
            .map(data => ({
                planName: data.planId?.planName,
                email: data.userId.email,
                firstName: data.userId.firstName,
                lastName: data.userId.lastName,
                createdAt: data.userId.createdAt,
                userId: data.userId._id,
                revenue: 0,
                purchaseDate: null
            }));

        const promises = filteredUserDetails.map(async data => {
            const transactionDetails = await Transaction.find({ userId: data.userId }).sort({ _id: -1 });
            if (transactionDetails.length !== 0) {
                if (transactionDetails[0].paymentGateway === "stripe" || transactionDetails[0].paymentGateway === undefined) {
                    if (transactionDetails[0].paymentInfo.currency === "inr") {
                        let inrPrice = transactionDetails[0].paymentInfo.amount_total / 100

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrPrice * percentage) / 100
                        data.revenue = percentageAmount * usdExchangeRate
                        data.purchaseDate = transactionDetails[0].createdAt
                    } else {
                        let usdPrice = transactionDetails[0].paymentInfo.amount_total / 100
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        data.revenue = percentageAmount
                        data.purchaseDate = transactionDetails[0].createdAt
                    }
                } else if (transactionDetails[0].paymentGateway === "ccavenue") {
                    if (transactionDetails[0].paymentInfo.currency.trim() === "INR") {
                        let inrPrice = parseInt(transactionDetails[0].paymentInfo.amount)

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrPrice * percentage) / 100
                        data.revenue = percentageAmount * usdExchangeRate
                        data.purchaseDate = transactionDetails[0].createdAt
                    } else {
                        let usdPrice = parseInt(transactionDetails[0].paymentInfo.amount)
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        data.revenue = percentageAmount
                        data.purchaseDate = transactionDetails[0].createdAt
                    }
                } else {
                    // we are not taking inr price because stripe does not support that
                    let usdPrice = parseInt(transactionDetails[0].paymentInfo.transactions[0].amount.total)
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    data.revenue += percentageAmount
                    data.purchaseDate = transactionDetails[0].createdAt
                }
            }

            return data
        })

        filteredUserDetails = await Promise.all(promises);

        res.status(200).json({
            usersDetails: filteredUserDetails,
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.get("/customer/referral/:id/:days", async (req, res) => {
    try {
        const days = parseInt(req.params.days);
        const startDate = new Date();
        startDate.setDate(startDate.getDate() - (days - 1));

        let dateArray = Array.from({ length: days }, (_, index) => {
            const currentDate = new Date(startDate);
            currentDate.setDate(currentDate.getDate() + index);

            return {
                date: currentDate,
                clicks: 0,
                signups: 0,
                customers: 0,
                revenue: 0
            };
        });

        const currentDate = new Date(startDate);
        currentDate.setHours(0, 0, 0, 0);

        const afilliateData = await Afilliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 })
            .populate({
                path: "afilliateUserAdded",
                match: { createdAt: { $gte: currentDate } },
                select: "_id"
            });

        const afilliateIds = afilliateData.afilliateUserAdded.map(user => user._id);

        const userPlanDetails = await UserFeatureFactory.find({ userId: { $in: afilliateIds } })
            .populate({ path: 'planId', select: 'planName -_id' })
            .populate({ path: 'userId', select: 'email lastName firstName createdAt' });

        let linkClickedData = await LinkClicked.findOne({ userId: req.params.id, days: { $gte: currentDate } });

        const filteredUserDetails = userPlanDetails
            .filter(data => data.userId && data.userId.email)
            .map(data => ({
                userId: data.userId._id,
                planName: data.planId?.planName,
                email: data.userId.email,
                firstName: data.userId.firstName,
                lastName: data.userId.lastName,
                createdAt: data.userId.createdAt
            }));

        const promises = dateArray.map(async (data) => {
            await Promise.all(filteredUserDetails.map(async (userDetails) => {
                const dateData = data.date.toDateString();
                const userDetailsDate = userDetails.createdAt.toDateString();

                if (dateData === userDetailsDate) {
                    data.signups++;
                    if (userDetails.planName !== "free") {
                        data.customers++;
                        const transactionDetails = await Transaction.find({ userId: userDetails.userId }).sort({ _id: -1 });
                        if (transactionDetails.length !== 0) {
                            if (transactionDetails[0].paymentGateway === "stripe" || transactionDetails[0].paymentGateway === undefined) {
                                if (transactionDetails[0].paymentInfo.currency === "inr") {
                                    let inrPrice = transactionDetails[0].paymentInfo.amount_total / 100

                                    const percentage = 15
                                    const usdExchangeRate = 0.012
                                    const percentageAmount = (inrPrice * percentage) / 100
                                    data.revenue = percentageAmount * usdExchangeRate
                                } else {
                                    let usdPrice = transactionDetails[0].paymentInfo.amount_total / 100
                                    const percentage = 15
                                    const percentageAmount = (usdPrice * percentage) / 100
                                    data.revenue = percentageAmount
                                }
                            } else if (transactionDetails[0].paymentGateway === "ccavenue") {
                                if (transactionDetails[0].paymentInfo.currency.trim() === "INR") {
                                    let inrPrice = parseInt(transactionDetails[0].paymentInfo.amount)

                                    const percentage = 15
                                    const usdExchangeRate = 0.012
                                    const percentageAmount = (inrPrice * percentage) / 100
                                    data.revenue = percentageAmount * usdExchangeRate
                                } else {
                                    let usdPrice = parseInt(transactionDetails[0].paymentInfo.amount)
                                    const percentage = 15
                                    const percentageAmount = (usdPrice * percentage) / 100
                                    data.revenue = percentageAmount
                                }
                            } else {
                                // we are not taking inr price because stripe does not support that
                                let usdPrice = parseInt(transactionDetails[0].paymentInfo.transactions[0].amount.total)
                                const percentage = 15
                                const percentageAmount = (usdPrice * percentage) / 100
                                data.revenue += percentageAmount
                            }
                        }
                    }
                }
            }));

            return data
        });

        dateArray = await Promise.all(promises);

        if (linkClickedData !== null) {
            dateArray.forEach(data => {
                linkClickedData.days.forEach(clickedDate => {
                    if (data.date.toDateString() === clickedDate.toDateString()) {
                        data.clicks++;
                    }
                });
            });
        }

        res.status(200).json({ dateArray });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
    }
});


router.get("/customer/sub/affiliate/:id/:days", async (req, res) => {
    try {
        const days = parseInt(req.params.days);
        const startDate = new Date();
        startDate.setDate(startDate.getDate() - (days - 1));

        let dateArray = Array.from({ length: days }, (_, index) => {
            const currentDate = new Date(startDate);
            currentDate.setDate(currentDate.getDate() + index);

            return {
                date: currentDate,
                clicks: 0,
                signups: 0,
                customers: 0,
                revenue: 0
            };
        });

        const currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - parseInt(req.params.days));
        currentDate.setHours(0, 0, 0, 0);

        // Find afilliate data based on user ID, excluding unnecessary fields & Extract afilliate user IDs
        const afilliateData = await Afilliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 }).populate({
            path: "afilliateUserAdded",
            match: {
                createdAt: { $gte: currentDate },
                isAffiliate: true
            },
            select: "_id"
        })

        const afilliateIds = afilliateData.afilliateUserAdded.map(user => user._id);

        // Find user plan details based on extracted IDs, populate necessary fields
        const userPlanDetails = await UserFeatureFactory.find({ userId: { $in: afilliateIds } })
            .populate({ path: 'planId', select: 'planName -_id' })
            .populate({ path: 'userId', select: 'email lastName firstName createdAt' });

        // Filter and format user details, excluding users without email
        const filteredUserDetails = userPlanDetails
            .filter(data => data.userId && data.userId.email)
            .map(data => ({
                userId: data.userId._id,
                planName: data.planId?.planName,
                email: data.userId.email,
                firstName: data.userId.firstName,
                lastName: data.userId.lastName,
                createdAt: data.userId.createdAt
            }));

        const promises = dateArray.map(async (data) => {
            const dateData = data.date.toDateString();
            const filteredUserDetailsPromises = filteredUserDetails.map(async (userDetails) => {
                const userDetailsDate = userDetails.createdAt.toDateString();

                if (dateData === userDetailsDate) {
                    data.signups++;

                    data.revenue = await getUserRevenueByDate(userDetails);
                    if (userDetails.planName !== "free") {
                        data.customers++
                    }
                }
            });

            await Promise.all(filteredUserDetailsPromises);
            return data;
        });

        dateArray = await Promise.all(promises);

        res.status(200).json({
            dateArray: dateArray,
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: "Internal Server Error" });
    }
})

const getUserRevenueByDate = async (userData) => {
    const firstDayOfMonth = new Date(); // Creates a new Date object with the current date and time
    firstDayOfMonth.setDate(1); // Sets the day of the month to 1
    firstDayOfMonth.setHours(0, 0, 0, 0);

    // Find afilliate data based on user ID, excluding unnecessary fields & Extract afilliate user IDs
    const afilliateData = await Afilliate.findOne({ userId: userData.userId }, { _id: 0, __v: 0, userId: 0 });
    const afilliateIds = afilliateData.afilliateUserAdded.map(user => user._id);

    // Find user plan details based on extracted IDs, populate necessary fields
    const userPlanDetails = await UserFeatureFactory.find({ userId: { $in: afilliateIds } })
        .populate({ path: 'planId', select: 'planName -_id' })
        .populate({
            path: 'userId',
            match: {
                createdAt: { $gt: firstDayOfMonth }
            },
            select: 'email lastName firstName createdAt'
        });

    // Filter and format user details, excluding users without email
    const filteredUserDetails = userPlanDetails
        .filter(data => data.userId && data.userId.email)
        .map(data => ({
            userId: data.userId._id,
            planName: data.planId?.planName,
            email: data.userId.email,
        }));


    let revenue = 0
    let salesCount = 0
    const promises = filteredUserDetails.map(async data => {
        const transactionDetails = await Transaction.find({ userId: data.userId, createdAt: { $gt: firstDayOfMonth } }).sort({ _id: -1 });

        if (transactionDetails.length !== 0) {
            salesCount++

            if (transactionDetails[0].paymentGateway === "stripe" || transactionDetails[0].paymentGateway === undefined) {
                if (transactionDetails[0].paymentInfo.currency === "inr") {
                    let inrPrice = transactionDetails[0].paymentInfo.amount_total / 100

                    const percentage = 15
                    const usdExchangeRate = 0.012
                    const percentageAmount = (inrPrice * percentage) / 100
                    revenue += percentageAmount * usdExchangeRate
                } else {
                    let usdPrice = transactionDetails[0].paymentInfo.amount_total / 100
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    revenue += percentageAmount
                }
            } else if (transactionDetails[0].paymentGateway === "ccavenue") {
                if (transactionDetails[0].paymentInfo.currency.trim() === "INR") {
                    let inrPrice = parseInt(transactionDetails[0].paymentInfo.amount)

                    const percentage = 15
                    const usdExchangeRate = 0.012
                    const percentageAmount = (inrPrice * percentage) / 100
                    revenue = percentageAmount * usdExchangeRate
                } else {
                    let usdPrice = parseInt(transactionDetails[0].paymentInfo.amount)
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    revenue = percentageAmount
                }
            } else {
                // we are not taking inr price because stripe does not support that
                let usdPrice = parseInt(transactionDetails[0].paymentInfo.transactions[0].amount.total)
                const percentage = 15
                const percentageAmount = (usdPrice * percentage) / 100
                revenue += percentageAmount
            }
        }

        return data
    })

    await Promise.all(promises);

    const currentDate = new Date(); // Creates a new Date object with the current date and time
    currentDate.setDate(currentDate.getDate() + 45);
    currentDate.setHours(0, 0, 0, 0);

    let cutPercentage = 5
    return (revenue * cutPercentage) / 100   // 5% to subaffiliate made by the partner affiliate
}


const getSubAffiliateRevenue = async (userId) => {
    const days = parseInt(30);
    const startDate = new Date();
    startDate.setDate(startDate.getDate() - (days - 1));

    let dateArray = Array.from({ length: days }, (_, index) => {
        const currentDate = new Date(startDate);
        currentDate.setDate(currentDate.getDate() + index);

        return {
            date: currentDate,
            clicks: 0,
            signups: 0,
            customers: 0,
            revenue: 0
        };
    });

    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - parseInt(30));
    currentDate.setHours(0, 0, 0, 0);

    // Find afilliate data based on user ID, excluding unnecessary fields & Extract afilliate user IDs
    const afilliateData = await Afilliate.findOne({ userId: userId }, { _id: 0, __v: 0, userId: 0 }).populate({
        path: "afilliateUserAdded",
        match: {
            createdAt: { $gte: currentDate },
            isAffiliate: true
        },
        select: "_id"
    })

    const afilliateIds = afilliateData.afilliateUserAdded.map(user => user._id);

    // Find user plan details based on extracted IDs, populate necessary fields
    const userPlanDetails = await UserFeatureFactory.find({ userId: { $in: afilliateIds } })
        .populate({ path: 'planId', select: 'planName -_id' })
        .populate({ path: 'userId', select: 'email lastName firstName createdAt' });

    // Filter and format user details, excluding users without email
    const filteredUserDetails = userPlanDetails
        .filter(data => data.userId && data.userId.email)
        .map(data => ({
            userId: data.userId._id,
            planName: data.planId?.planName,
            email: data.userId.email,
            firstName: data.userId.firstName,
            lastName: data.userId.lastName,
            createdAt: data.userId.createdAt
        }));

    const promises = dateArray.map(async (data) => {
        const dateData = data.date.toDateString();
        const filteredUserDetailsPromises = filteredUserDetails.map(async (userDetails) => {
            const userDetailsDate = userDetails.createdAt.toDateString();

            if (dateData === userDetailsDate) {
                data.signups++;

                data.revenue = await getUserRevenueByDate(userDetails);
                if (userDetails.planName !== "free") {
                    data.customers++
                }
            }
        });

        await Promise.all(filteredUserDetailsPromises);
        return data;
    });

    dateArray = await Promise.all(promises);
    let revenue = 0
    dateArray.forEach(data => {
        revenue += data.revenue
    })

    return revenue
}

router.get("/payout/details/:id", async (req, res) => {
    try {
        const firstDayOfMonth = new Date(); // Creates a new Date object with the current date and time
        firstDayOfMonth.setDate(1); // Sets the day of the month to 1
        firstDayOfMonth.setHours(0, 0, 0, 0);

        // Find afilliate data based on user ID, excluding unnecessary fields & Extract afilliate user IDs
        const afilliateData = await Afilliate.findOne({ userId: req.params.id }, { _id: 0, __v: 0, userId: 0 });
        const afilliateIds = afilliateData.afilliateUserAdded.map(user => user._id);

        // Find user plan details based on extracted IDs, populate necessary fields
        const userPlanDetails = await UserFeatureFactory.find({ userId: { $in: afilliateIds } })
            .populate({ path: 'planId', select: 'planName -_id' })
            .populate({
                path: 'userId',
                match: {
                    createdAt: { $gt: firstDayOfMonth }
                },
                select: 'email lastName firstName createdAt'
            });

        // Filter and format user details, excluding users without email
        const filteredUserDetails = userPlanDetails
            .filter(data => data.userId && data.userId.email)
            .map(data => ({
                userId: data.userId._id,
                planName: data.planId?.planName,
                email: data.userId.email,
            }));


        let revenue = 0
        let salesCount = 0
        const promises = filteredUserDetails.map(async data => {
            const transactionDetails = await Transaction.find({ userId: data.userId, createdAt: { $gt: firstDayOfMonth } }).sort({ _id: -1 });

            if (transactionDetails.length !== 0) {
                salesCount++

                if (transactionDetails[0].paymentGateway === "stripe" || transactionDetails[0].paymentGateway === undefined) {
                    if (transactionDetails[0].paymentInfo.currency === "inr") {
                        let inrPrice = transactionDetails[0].paymentInfo.amount_total / 100

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrPrice * percentage) / 100
                        revenue += percentageAmount * usdExchangeRate
                    } else {
                        let usdPrice = transactionDetails[0].paymentInfo.amount_total / 100
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        revenue += percentageAmount
                    }
                } else if (transactionDetails[0].paymentGateway === "ccavenue") {
                    if (transactionDetails[0].paymentInfo.currency.trim() === "INR") {
                        let inrPrice = parseInt(transactionDetails[0].paymentInfo.amount)

                        const percentage = 15
                        const usdExchangeRate = 0.012
                        const percentageAmount = (inrPrice * percentage) / 100
                        revenue += percentageAmount * usdExchangeRate
                    } else {
                        let usdPrice = parseInt(transactionDetails[0].paymentInfo.amount)
                        const percentage = 15
                        const percentageAmount = (usdPrice * percentage) / 100
                        revenue += percentageAmount
                    }
                } else {
                    // we are not taking inr price because stripe does not support that
                    let usdPrice = parseInt(transactionDetails[0].paymentInfo.transactions[0].amount.total)
                    const percentage = 15
                    const percentageAmount = (usdPrice * percentage) / 100
                    revenue += percentageAmount
                }
            }

            return data
        })

        await Promise.all(promises);

        const currentDate = new Date(); // Creates a new Date object with the current date and time
        currentDate.setDate(currentDate.getDate() + 50);
        currentDate.setHours(0, 0, 0, 0);

        let cutRevenue = await getSubAffiliateRevenue(req.params.id)

        res.status(200).json({
            startDate: firstDayOfMonth,
            payoutDate: currentDate,
            salesCount: salesCount,
            revenue: revenue + cutRevenue
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: "Internal Server Error" });
    }
})

module.exports = router