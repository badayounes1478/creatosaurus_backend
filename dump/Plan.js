const express = require('express');
const router = express.Router();
const {
Createplan,
Deleteplan,
Getallplan,
GetplanasperId,
updatePlan
} = require('../Controller/Plan');
  
router.post('/create', Createplan);

router.delete('/deletePlan/:plan_name', Deleteplan);
 
router.get('/getallPlan',Getallplan);
 
router.post('/getallasperid',GetplanasperId);

router.put('/updatePlan',updatePlan)


module.exports = router;
