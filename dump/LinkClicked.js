const express = require('express')
const router = express.Router()
const LinkClicked = require('../Schema/LinkClicked')
const ObjectId = require('mongoose').Types.ObjectId;

router.post("/clicked", async (req, res) => {
    try {
        const { userId } = req.body;

        if (!userId || !ObjectId.isValid(userId)) {
            return res.status(400).json({ error: "Missing userId in the request body" });
        }

        // Find the user and update the 'days' array
        await LinkClicked.findOneAndUpdate(
            { userId },
            { $push: { days: new Date() } },
            { upsert: true, new: true }
        );

        return res.status(200).json({});
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: "Internal server error" });
    }
})

module.exports = router