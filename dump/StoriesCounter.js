const express = require('express');
const { counter, getStoriesCount } = require('../Controller/StoriesCounter');
const router = express.Router();

router.post("/add/story", counter)

router.get("/get/story", getStoriesCount)

module.exports = router;