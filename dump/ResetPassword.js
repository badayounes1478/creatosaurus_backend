const express = require('express');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const sesTransport = require('nodemailer-ses-transport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const user = require('../Schema/User');
const router = express.Router();
const logger = require("../logger");

const transporter = nodemailer.createTransport(sesTransport({
    accessKeyId: 'AKIAQ5RO3HORQD7AMD4E',
    secretAccessKey: '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',
    region: 'ap-south-1',
    rateLimit: 5
}));

router.post('/reset', async (req, res) => {
    try {
        // check email is there or not
        if (req.body.email === "") {
            return res.status(400).json({
                error: 'email required'
            });
        }

        const userIsThere = await user.findOne({ email: req.body.email });
        if (userIsThere === null) {
            return res.status(404).json({
                error: `User with ${req.body.email} doesn't exists.`
            });
        } else {
            const token = crypto.randomBytes(20).toString('hex');

            await userIsThere.updateOne({
                resetPasswordToken: token,
                resetPasswordExpires: Date.now() + 3600000
            });

            const mailOptions = {
                from: 'Creatosaurus <account@creatosaurus.io>',
                to: req.body.email,
                subject: 'Reset your Creatosaurus password 👍🏻',
                html: `<!doctype html>
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <title>
          
        </title>
        <!--[if !mso]><!-- -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
          #outlook a { padding:0; }
          body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
          table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
          img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
          p { display:block;margin:13px 0; }
        </style>
        <!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <!--[if lte mso 11]>
        <style type="text/css">
          .outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
        
      <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet" type="text/css">
        <style type="text/css">
          @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
@import url(https://fonts.googleapis.com/css?family=Poppins:400,700);
        </style>
      <!--<![endif]-->

    
        
    <style type="text/css">
      @media only screen and (max-width:480px) {
        .mj-column-per-100 { width:100% !important; max-width: 100%; }
      }
    </style>
    
  
        <style type="text/css">
        
        

    @media only screen and (max-width:480px) {
      table.full-width-mobile { width: 100% !important; }
      td.full-width-mobile { width: auto !important; }
    }
  
        </style>
        <style type="text/css">.hide_on_mobile { display: none !important;} 
        @media only screen and (min-width: 480px) { .hide_on_mobile { display: block !important;} }
        .hide_section_on_mobile { display: none !important;} 
        @media only screen and (min-width: 480px) { 
            .hide_section_on_mobile { 
                display: table !important;
            } 

            div.hide_section_on_mobile { 
                display: block !important;
            }
        }
        .hide_on_desktop { display: block !important;} 
        @media only screen and (min-width: 480px) { .hide_on_desktop { display: none !important;} }
        .hide_section_on_desktop { 
            display: table !important;
            width: 100%;
        } 
        @media only screen and (min-width: 480px) { .hide_section_on_desktop { display: none !important;} }
        
          p, h1, h2, h3 {
              margin: 0px;
          }

          ul, li, ol {
            font-size: 11px;
            font-family: Ubuntu, Helvetica, Arial;
          }

          a {
              text-decoration: none;
              color: inherit;
          }

          @media only screen and (max-width:480px) {

            .mj-column-per-100 { width:100%!important; max-width:100%!important; }
            .mj-column-per-100 > .mj-column-per-75 { width:75%!important; max-width:75%!important; }
            .mj-column-per-100 > .mj-column-per-60 { width:60%!important; max-width:60%!important; }
            .mj-column-per-100 > .mj-column-per-50 { width:50%!important; max-width:50%!important; }
            .mj-column-per-100 > .mj-column-per-40 { width:40%!important; max-width:40%!important; }
            .mj-column-per-100 > .mj-column-per-33 { width:33.333333%!important; max-width:33.333333%!important; }
            .mj-column-per-100 > .mj-column-per-25 { width:25%!important; max-width:25%!important; }

            .mj-column-per-100 { width:100%!important; max-width:100%!important; }
            .mj-column-per-75 { width:100%!important; max-width:100%!important; }
            .mj-column-per-60 { width:100%!important; max-width:100%!important; }
            .mj-column-per-50 { width:100%!important; max-width:100%!important; }
            .mj-column-per-40 { width:100%!important; max-width:100%!important; }
            .mj-column-per-33 { width:100%!important; max-width:100%!important; }
            .mj-column-per-25 { width:100%!important; max-width:100%!important; }
        }</style>
        
      </head>
      <body style="background-color:#F8F8F9;">
        
    <div style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
      Forgot your password? No worries. Let us help you retrieve it.
    </div>
  
        
      <div style="background-color:#F8F8F9;">
        
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#FFFFFF;background-color:#FFFFFF;width:100%;">
        <tbody>
          <tr>
            <td>
              
        
      <!--[if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    
        
      <div style="margin:0px auto;max-width:600px;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td style="direction:ltr;font-size:0px;padding:0px 0px 0px 0px;text-align:center;">
                <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:600px;"
            >
          <![endif]-->
            
      <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="30" style="vertical-align:top;height:30px;">
      
    <![endif]-->
  
      <div style="height:30px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
            <tr>
              <td align="center" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;">
                
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
        <tbody>
          <tr>
            <td style="width:48px;">
              
        <a href="https://www.creatosaurus.io/" target="_blank" style="color: #0000EE;">
          
      <img alt="Creatosaurus" height="auto" src="https://s3-eu-west-1.amazonaws.com/topolio/uploads/634e334873887/1666069362.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="48">
    
        </a>
      
            </td>
          </tr>
        </tbody>
      </table>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><strong>CREATOSAURUS</strong></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="10" style="vertical-align:top;height:10px;">
      
    <![endif]-->
  
      <div style="height:10px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 5px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><h1 style="font-family: Poppins, sans-serif; font-size: 22px; text-align: center;">Forgot your password?</h1></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:5px 20px 5px 20px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="font-size: 13px; color: rgb(0, 0, 0);">Rest your password by clicking the button below👇🏻</span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="10" style="vertical-align:top;height:10px;">
      
    <![endif]-->
  
      <div style="height:10px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
            <tr>
              <td align="center" vertical-align="middle" style="font-size:0px;padding:10px 10px 10px 10px;word-break:break-word;">
                
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:auto;line-height:100%;">
        <tr>
          <td align="center" bgcolor="#FF4359" role="presentation" style="border:0px #000000 solid;border-radius:3px;cursor:auto;mso-padding-alt:10px 30px 10px 30px;background:#FF4359;" valign="middle">
            <a href="https://www.app.creatosaurus.io/reset/${token}" style="display: inline-block; background: #FF4359; color: #ffffff; font-family: Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 100%; margin: 0; text-decoration: none; text-transform: none; padding: 10px 30px 10px 30px; mso-padding-alt: 0px; border-radius: 3px;" target="_blank">
              <span>Reset Password</span>
            </a>
          </td>
        </tr>
      </table>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="20" style="vertical-align:top;height:20px;">
      
    <![endif]-->
  
      <div style="height:20px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.6;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center; font-size: 13px;">or you can also copy and paste this URL into your browser<br><span style="color: rgb(0, 0, 0);"><a style="color: #0000EE;" href="https://www.app.creatosaurus.io/reset/${token}" target="_blank" rel="noopener"><span style="font-size: 13px;">https://www.app.creatosaurus.io/reset/${token}</span></a></span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:20px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94);">Any questions? Drop us a reply! </span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;padding:10px 10px;padding-top:10px;word-break:break-word;">
                
      <p style="font-family: Poppins, sans-serif; border-top: solid 1px #F5F5F5; font-size: 1; margin: 0px auto; width: 100%;">
      </p>
      
      <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #F5F5F5;font-size:1;margin:0px auto;width:580px;" role="presentation" width="580px"
        >
          <tr>
            <td style="height:0;line-height:0;">
              &nbsp;
            </td>
          </tr>
        </table>
      <![endif]-->
    
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">Follow us on</p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="center" style="font-size:0px;padding:5px 10px 5px 10px;word-break:break-word;">
                
      
     <!--[if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
      >
        <tr>
      
              <td>
            <![endif]-->
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                
      <tr>
        <td style="padding:4px;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
            <tr>
              <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                <a href="https://www.linkedin.com/company/creatosaurushq" target="_blank" style="color: #0000EE;">
                    <img alt="LinkedIn" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/linkedin.png" style="border-radius:3px;display:block;" width="20">
                  </a>
                </td>
              </tr>
          </table>
        </td>
        
      </tr>
    
              </table>
            <!--[if mso | IE]>
              </td>
            
              <td>
            <![endif]-->
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                
      <tr>
        <td style="padding:4px;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
            <tr>
              <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                <a href="https://twitter.com/home?status=https://twitter.com/creatosaurushq" target="_blank" style="color: #0000EE;">
                    <img alt="Twitter" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/twitter.png" style="border-radius:3px;display:block;" width="20">
                  </a>
                </td>
              </tr>
          </table>
        </td>
        
      </tr>
    
              </table>
            <!--[if mso | IE]>
              </td>
            
              <td>
            <![endif]-->
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                
      <tr>
        <td style="padding:4px;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
            <tr>
              <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                <a href="https://www.facebook.com/creatosaurushq" target="_blank" style="color: #0000EE;">
                    <img alt="Facebook" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/facebook.png" style="border-radius:3px;display:block;" width="20">
                  </a>
                </td>
              </tr>
          </table>
        </td>
        
      </tr>
    
              </table>
            <!--[if mso | IE]>
              </td>
            
              <td>
            <![endif]-->
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                
      <tr>
        <td style="padding:4px;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
            <tr>
              <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                <a href="https://www.instagram.com/creatosaurus/" target="_blank" style="color: #0000EE;">
                    <img alt="Instagram" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/instagram.png" style="border-radius:3px;display:block;" width="20">
                  </a>
                </td>
              </tr>
          </table>
        </td>
        
      </tr>
    
              </table>
            <!--[if mso | IE]>
              </td>
            
          </tr>
        </table>
      <![endif]-->
    
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94);">Creatosaurus, 6th Floor, Ideas to Impacts Hub, Baner, Pune, India.<br><a href="https://www.creatosaurus.io/" target="_blank" rel="noopener" style="color: #0000EE;">creatosaurus.io</a></span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:5px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94); background-color: rgb(255, 255, 255);"><a href="https://www.creatosaurus.io/blog" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Our Blog</a>  |  <a href="https://www.creatosaurus.io/contact" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Contact</a>  |  <a href="https://www.creatosaurus.io/terms" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Terms</a>  |  <a href="https://www.creatosaurus.io/privacy" target="_blank" rel="noopener" style="color:rgb(126, 140, 141)">Privacy</a>  |  <a href="https://www.app.creatosaurus.io/help" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Help Center</a>  |  <a href="https://www.creatosaurus.io" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Creatosaurus Community</a><br></span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:5px 15px 5px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><a href="https://www.app.creatosaurus.io/settings" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)"><span style="text-decoration: underline; color: rgb(126, 140, 141);">Manage your email preference</span></a><span style="color: rgb(126, 140, 141);">   |   <a href="https://www.app.creatosaurus.io/unsubscribe/${userIsThere._id}" target="_blank" rel="noopener" style="color: rgb(126, 140, 141);"><span style="text-decoration: underline;">Unsubscribe</span></a></span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(126, 140, 141); background-color: rgb(255, 255, 255);">© 2022 Creatosaurus. All rights reserved by Creatosaurus</span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">Made with ❤️ for Creators</p></div>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="40" style="vertical-align:top;height:40px;">
      
    <![endif]-->
  
      <div style="height:40px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
      </table>
    
      </div>
    
          <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        
      </div>
    
        
      <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
    
      
            </td>
          </tr>
        </tbody>
      </table>
    
      </div>
    
      </body>
    </html>`};

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    res.status(200).json({
                        message: "recovery email sent"
                    });
                }
            });
        }
    } catch (error) {
        logger.error(error);
        res.status(500).json({
            error: error
        });
    }
});

router.post('/reset/update', async (req, res) => {
    try {
        const data = await user.findOne({ resetPasswordToken: req.body.token });
        if (data === null) {
            return res.status(400).json({
                error: "invalid token"
            });
        } else {
            const result = await bcrypt.compare(req.body.password, data.password);
            if (result) return res.status(200).json({ message: "old password enter." })
            data.failedLoginAttempts = 0;
            data.password = await bcrypt.hash(req.body.password, 10);
            data.resetPasswordToken = "";
            await data.save();

            const mailOptions = {
                from: 'Creatosaurus <account@creatosaurus.io>',
                to: data.email,
                subject: 'Creatosaurus password changed successfully 🙌🏻',
                html: `<!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
          <head>
            <title>
              
            </title>
            <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <style type="text/css">
              #outlook a { padding:0; }
              body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
              table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
              img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
              p { display:block;margin:13px 0; }
            </style>
            <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
              <o:AllowPNG/>
              <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
            <style type="text/css">
              .outlook-group-fix { width:100% !important; }
            </style>
            <![endif]-->
            
          <!--[if !mso]><!-->
            <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet" type="text/css">
            <style type="text/css">
              @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
    @import url(https://fonts.googleapis.com/css?family=Poppins:400,700);
            </style>
          <!--<![endif]-->
    
        
            
        <style type="text/css">
          @media only screen and (max-width:480px) {
            .mj-column-per-100 { width:100% !important; max-width: 100%; }
          }
        </style>
        
      
            <style type="text/css">
            
            
    
        @media only screen and (max-width:480px) {
          table.full-width-mobile { width: 100% !important; }
          td.full-width-mobile { width: auto !important; }
        }
      
            </style>
            <style type="text/css">.hide_on_mobile { display: none !important;} 
            @media only screen and (min-width: 480px) { .hide_on_mobile { display: block !important;} }
            .hide_section_on_mobile { display: none !important;} 
            @media only screen and (min-width: 480px) { 
                .hide_section_on_mobile { 
                    display: table !important;
                } 
    
                div.hide_section_on_mobile { 
                    display: block !important;
                }
            }
            .hide_on_desktop { display: block !important;} 
            @media only screen and (min-width: 480px) { .hide_on_desktop { display: none !important;} }
            .hide_section_on_desktop { 
                display: table !important;
                width: 100%;
            } 
            @media only screen and (min-width: 480px) { .hide_section_on_desktop { display: none !important;} }
            
              p, h1, h2, h3 {
                  margin: 0px;
              }
    
              ul, li, ol {
                font-size: 11px;
                font-family: Ubuntu, Helvetica, Arial;
              }
    
              a {
                  text-decoration: none;
                  color: inherit;
              }
    
              @media only screen and (max-width:480px) {
    
                .mj-column-per-100 { width:100%!important; max-width:100%!important; }
                .mj-column-per-100 > .mj-column-per-75 { width:75%!important; max-width:75%!important; }
                .mj-column-per-100 > .mj-column-per-60 { width:60%!important; max-width:60%!important; }
                .mj-column-per-100 > .mj-column-per-50 { width:50%!important; max-width:50%!important; }
                .mj-column-per-100 > .mj-column-per-40 { width:40%!important; max-width:40%!important; }
                .mj-column-per-100 > .mj-column-per-33 { width:33.333333%!important; max-width:33.333333%!important; }
                .mj-column-per-100 > .mj-column-per-25 { width:25%!important; max-width:25%!important; }
    
                .mj-column-per-100 { width:100%!important; max-width:100%!important; }
                .mj-column-per-75 { width:100%!important; max-width:100%!important; }
                .mj-column-per-60 { width:100%!important; max-width:100%!important; }
                .mj-column-per-50 { width:100%!important; max-width:100%!important; }
                .mj-column-per-40 { width:100%!important; max-width:100%!important; }
                .mj-column-per-33 { width:100%!important; max-width:100%!important; }
                .mj-column-per-25 { width:100%!important; max-width:100%!important; }
            }</style>
            
          </head>
          <body style="background-color:#F8F8F9;">
            
        <div style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
          Your password has been successfully changed for Creatosaurus.
        </div>
      
            
          <div style="background-color:#F8F8F9;">
            
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#FFFFFF;background-color:#FFFFFF;width:100%;">
            <tbody>
              <tr>
                <td>
                  
            
          <!--[if mso | IE]>
          <table
             align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
          >
            <tr>
              <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
          <![endif]-->
        
            
          <div style="margin:0px auto;max-width:600px;">
            
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
              <tbody>
                <tr>
                  <td style="direction:ltr;font-size:0px;padding:0px 0px 0px 0px;text-align:center;">
                    <!--[if mso | IE]>
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    
            <tr>
          
                <td
                   class="" style="vertical-align:top;width:600px;"
                >
              <![endif]-->
                
          <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
            
                <tr>
                  <td style="font-size:0px;word-break:break-word;">
                    
          
        <!--[if mso | IE]>
        
            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="30" style="vertical-align:top;height:30px;">
          
        <![endif]-->
      
          <div style="height:30px;">
            &nbsp;
          </div>
          
        <!--[if mso | IE]>
        
            </td></tr></table>
          
        <![endif]-->
      
        
                  </td>
                </tr>
              
                <tr>
                  <td align="center" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;">
                    
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
            <tbody>
              <tr>
                <td style="width:48px;">
                  
            <a href="https://www.creatosaurus.io/" target="_blank" style="color: #0000EE;">
              
          <img alt="Creatosaurus" height="auto" src="https://s3-eu-west-1.amazonaws.com/topolio/uploads/634e334873887/1666069362.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="48">
        
            </a>
          
                </td>
              </tr>
            </tbody>
          </table>
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><strong>CREATOSAURUS</strong></p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td style="font-size:0px;word-break:break-word;">
                    
          
        <!--[if mso | IE]>
        
            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="10" style="vertical-align:top;height:10px;">
          
        <![endif]-->
      
          <div style="height:10px;">
            &nbsp;
          </div>
          
        <!--[if mso | IE]>
        
            </td></tr></table>
          
        <![endif]-->
      
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:15px 15px 5px 15px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><h1 style="font-family: Poppins, sans-serif; font-size: 22px; text-align: center;">Password reset successfully</h1></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:5px 20px 5px 20px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">You have successfully reset your Creatosaurus login password.</p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td style="font-size:0px;word-break:break-word;">
                    
          
        <!--[if mso | IE]>
        
            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="10" style="vertical-align:top;height:10px;">
          
        <![endif]-->
      
          <div style="height:10px;">
            &nbsp;
          </div>
          
        <!--[if mso | IE]>
        
            </td></tr></table>
          
        <![endif]-->
      
        
                  </td>
                </tr>
              
                <tr>
                  <td style="font-size:0px;word-break:break-word;">
                    
          
        <!--[if mso | IE]>
        
            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="20" style="vertical-align:top;height:20px;">
          
        <![endif]-->
      
          <div style="height:20px;">
            &nbsp;
          </div>
          
        <!--[if mso | IE]>
        
            </td></tr></table>
          
        <![endif]-->
      
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:20px 15px 15px 15px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94);">Any questions? Drop us a reply! </span></p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td style="font-size:0px;padding:10px 10px;padding-top:10px;word-break:break-word;">
                    
          <p style="font-family: Poppins, sans-serif; border-top: solid 1px #F5F5F5; font-size: 1; margin: 0px auto; width: 100%;">
          </p>
          
          <!--[if mso | IE]>
            <table
               align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #F5F5F5;font-size:1;margin:0px auto;width:580px;" role="presentation" width="580px"
            >
              <tr>
                <td style="height:0;line-height:0;">
                  &nbsp;
                </td>
              </tr>
            </table>
          <![endif]-->
        
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                    
                    <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">Follow us on</p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td align="center" style="font-size:0px;padding:5px 10px 5px 10px;word-break:break-word;">
                    
          
         <!--[if mso | IE]>
          <table
             align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
          >
            <tr>
          
                  <td>
                <![endif]-->
                  <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                    
          <tr>
            <td style="padding:4px;">
              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
                <tr>
                  <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                    <a href="https://www.linkedin.com/company/creatosaurushq" target="_blank" style="color: #0000EE;">
                        <img alt="LinkedIn" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/linkedin.png" style="border-radius:3px;display:block;" width="20">
                      </a>
                    </td>
                  </tr>
              </table>
            </td>
            
          </tr>
        
                  </table>
                <!--[if mso | IE]>
                  </td>
                
                  <td>
                <![endif]-->
                  <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                    
          <tr>
            <td style="padding:4px;">
              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
                <tr>
                  <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                    <a href="https://twitter.com/home?status=https://twitter.com/creatosaurushq" target="_blank" style="color: #0000EE;">
                        <img alt="Twitter" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/twitter.png" style="border-radius:3px;display:block;" width="20">
                      </a>
                    </td>
                  </tr>
              </table>
            </td>
            
          </tr>
        
                  </table>
                <!--[if mso | IE]>
                  </td>
                
                  <td>
                <![endif]-->
                  <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                    
          <tr>
            <td style="padding:4px;">
              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
                <tr>
                  <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                    <a href="https://www.facebook.com/creatosaurushq" target="_blank" style="color: #0000EE;">
                        <img alt="Facebook" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/facebook.png" style="border-radius:3px;display:block;" width="20">
                      </a>
                    </td>
                  </tr>
              </table>
            </td>
            
          </tr>
        
                  </table>
                <!--[if mso | IE]>
                  </td>
                
                  <td>
                <![endif]-->
                  <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                    
          <tr>
            <td style="padding:4px;">
              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
                <tr>
                  <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                    <a href="https://www.instagram.com/creatosaurus/" target="_blank" style="color: #0000EE;">
                        <img alt="Instagram" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/instagram.png" style="border-radius:3px;display:block;" width="20">
                      </a>
                    </td>
                  </tr>
              </table>
            </td>
            
          </tr>
        
                  </table>
                <!--[if mso | IE]>
                  </td>
                
              </tr>
            </table>
          <![endif]-->
        
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94);">Creatosaurus, 6th Floor, Ideas to Impacts Hub, Baner, Pune, India.<br><a href="https://www.creatosaurus.io/" target="_blank" rel="noopener" style="color: #0000EE;">creatosaurus.io</a></span></p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:5px 15px 15px 15px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94); background-color: rgb(255, 255, 255);"><a href="https://www.creatosaurus.io/blog" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Our Blog</a>  |  <a href="https://www.creatosaurus.io/contact" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Contact</a>  |  <a href="https://www.creatosaurus.io/terms" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Terms</a>  |  <a href="https://www.creatosaurus.io/privacy" target="_blank" rel="noopener" style="color:rgb(126, 140, 141)">Privacy</a>  |  <a href="https://www.app.creatosaurus.io/help" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Help Center</a>  |  <a href="https://www.creatosaurus.io" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Creatosaurus Community</a><br></span></p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:5px 15px 5px 15px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><a href="https://www.app.creatosaurus.io/settings" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)"><span style="text-decoration: underline; color: rgb(126, 140, 141);">Manage your email preference</span></a><span style="color: rgb(126, 140, 141);">   |   <a href="https://www.app.creatosaurus.io/unsubscribe/${data._id}" target="_blank" rel="noopener" style="color: rgb(126, 140, 141);"><span style="text-decoration: underline;">Unsubscribe</span></a></span></p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(126, 140, 141); background-color: rgb(255, 255, 255);">© 2022 Creatosaurus. All rights reserved by Creatosaurus</span></p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                    
          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">Made with ❤️ for Creators</p></div>
        
                  </td>
                </tr>
              
                <tr>
                  <td style="font-size:0px;word-break:break-word;">
                    
          
        <!--[if mso | IE]>
        
            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="40" style="vertical-align:top;height:40px;">
          
        <![endif]-->
      
          <div style="height:40px;">
            &nbsp;
          </div>
          
        <!--[if mso | IE]>
        
            </td></tr></table>
          
        <![endif]-->
      
        
                  </td>
                </tr>
              
          </table>
        
          </div>
        
              <!--[if mso | IE]>
                </td>
              
            </tr>
          
                      </table>
                    <![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
            
          </div>
        
            
          <!--[if mso | IE]>
              </td>
            </tr>
          </table>
          <![endif]-->
        
          
                </td>
              </tr>
            </tbody>
          </table>
        
          </div>
        
          </body>
        </html>`};

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                }
            });

            const token = jwt.sign({
                data: data.email,
                isAdmin: data.isAdmin,
                id: data._id
            }, 'secret', { expiresIn: '14d' });

            return res.status(200).json({
                message: "authentication succesfull",
                token
            });
        }
    } catch (error) {
        logger.error(error);
        res.status(500).json({
            error: error
        });
    }
});

module.exports = router;