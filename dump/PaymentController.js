const payments = require('../Schema/Payment')
const logger = require("../logger")

const getAllPaymentHistory = async (req, res) => {
    try {
        const paymentsHistory = await payments.find({});
        return res.status(201).json({
            message: 'plan fetch sucessfully...',
            data: paymentsHistory
        });
    } catch (error) {
        logger.error(error)
        return res.status(500).json({
            error: error,
        });
    }
}

const insertPaymentDetails = async (req, res) => {
    try {
        if (req.body !== null) {
            const payment = await payments.create(req.body)
            return res.status(201).json({
                message: 'payment record added sucessfully...',
                data: payment
            });
        }

    } catch (error) {
        logger.error(error)
        return res.status(500).json({
            error: error
        })
    }
}

module.exports = {
    insertPaymentDetails,
    getAllPaymentHistory
}