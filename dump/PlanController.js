const plan = require('../Schema/Plan')
const logger = require("../logger")

const Getallplan = async (req, res) => {
  try {
    const planData = await plan.find({});
    return res.status(201).json({
      message: 'plan fetch sucessfully...',
      data: planData
    });
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const GetplanasperId = async (req, res) => {
  try {
    const planData = await plan.findOne({ _id: req.body.id });
    if (planData !== null) {
      return res.status(201).json({
        message: 'plan fetch sucessfully...',
        data: planData
      });
    } else {
      return res.status(401).json({
        error: 'plan id does not found...',
      });
    }
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const Createplan = async (req, res) => {
  try {
    // create the new user in data base and generate the token
    //console.log(req.body)
    const planData = await plan.create(req.body);

    return res.status(201).json({
      message: 'plan added sucessfully...',
      data: planData
    })
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const Deleteplan = async (req, res) => {
  try {
    if (req.params.plan_name !== null) {
      //console.log(req.params.plan_name)
      const planExist = await plan.findOne({ planName: req.params.plan_name });
      if (planExist !== null) {
        await plan.deleteOne({ planName: req.params.plan_name });
        return res.status(201).json({
          message: 'plan deleted sucessfully...',
        });
      } else {
        return res.status(401).json({
          error: 'plan name does not found...',
        });
      }
    }
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
};

const updatePlan = async (req, res) => {
  try {
    //console.log(req.body)
    if (req.body.id !== null) {
      const planExist = await plan.findOne({ _id: req.body.id });
      if (planExist !== null) {
        planExist.planName = req.body.plan_name;
        planExist.pricePerMonth = req.body.price_per_month;
        planExist.currencyType = req.body.currency_type;
        planExist.pricePerYear = req.body.price_per_year
        planExist.workspace = req.body.workspace
        planExist.users = req.body.users
        planExist.socialAccounts = req.body.socialAccounts
        planExist.schedulePost = req.body.schedulePost
        planExist.aiCredites = req.body.aiCredites
        planExist.hashtagSearch = req.body.hashtagSearch

        planExist.save()
        return res.status(200).json({
          message: 'updated successfully',
          data: planExist
        });
      }
      else {
        return res.status(401).json({
          error: 'plan name does not found...',
        });
      }
    }
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}


module.exports = {
  Createplan,
  Deleteplan,
  Getallplan,
  GetplanasperId,
  updatePlan
};
