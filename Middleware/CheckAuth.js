const jwt = require('jsonwebtoken')
const logger = require("../logger")

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization?.split(" ")[1]
        const decoded = jwt.verify(token, "kv1IDb96qqZnc0w0AINeNkCxvbqJsdDLklu76eKpIjY")
        req.body.userId = decoded.id
        next()
    } catch (error) {
        logger.error(error)
        return res.status(401).json({
            error: "Unauthorized"
        })
    }
}