const workspace = require('../Schema/Workspace')
const user = require('../Schema/User')
const nodemailer = require('nodemailer');
const sesTransport = require('nodemailer-ses-transport');
const logger = require("../logger")
const UserFeatureSettings = require('../Schema/UserFeatureSettings')

const transporter = nodemailer.createTransport(
  sesTransport({
    accessKeyId: 'AKIAQ5RO3HORQD7AMD4E',
    secretAccessKey: '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',
    region: 'ap-south-1',
    rateLimit: 5,
  })
);

const CreateWorkspace = async (req, res) => {
  try {
    if (req.body !== null) {
      const featureFactoryData = await UserFeatureSettings.findOne({ userId: req.body.userId })
      const worksapcesOfUser = await workspace.find({ user_id: req.body.userId })

      // do not create default workspace twice
      if (worksapcesOfUser.length !== 0 && req.body.default_personal === true) return res.status(400).json({
        error: "You already created your workspace"
      })

      let checkUserHasSameNameOfWorkspaceAsPreviousWorkspaces = false
      worksapcesOfUser.forEach(data => {
        if (data.workspace_name.toLowerCase().trim() === req.body.workspace_name.toLowerCase().trim()) {
          checkUserHasSameNameOfWorkspaceAsPreviousWorkspaces = true
        }
      })

      if (checkUserHasSameNameOfWorkspaceAsPreviousWorkspaces) return res.status(409).json({
        error: "The workspace name has been duplicated, please try another workspace name."
      })

      req.body.user_id = req.body.userId

      if (featureFactoryData.workspace === null) {
        // create workspace
        let userData = await user.findOne({ _id: req.body.userId })
        req.body.user_email = userData.email
        req.body.team = req.body.team.map(data => {
          if (data.role === "owner") {
            data.user_email = userData.email
          }
          return data
        })

        let workspaceData = await workspace.create(req.body)
        let planData = await UserFeatureSettings.findOne({ userId: workspaceData.user_id }).populate('planId')
        workspaceData = { ...workspaceData._doc, userPlanData: planData };

        // send email to team members
        workspaceData.team.map((data, index) => {
          if (data.user_email !== workspaceData.user_email) {
            sendEmail(workspaceData._id, req.body.team[index].user_email, workspaceData.workspace_name, req.body.user_name)
          }
        })

        workspaceData.team = await Promise.all(workspaceData.team.map(async data => {
          let userData = await user.findOne({ email: data.user_email })
          return { ...data._doc, firstName: userData?.firstName }
        }))

        return res.status(201).json({
          message: 'workspace added successfully...',
          data: workspaceData
        })
      } else {
        const workSpaceNumber = parseInt(featureFactoryData.workspace)
        const totalWorkSpace = await workspace.countDocuments({ user_id: req.body.userId })

        if (totalWorkSpace < workSpaceNumber) {
          // create workspace
          let userData = await user.findOne({ _id: req.body.userId })
          req.body.user_email = userData.email
          req.body.team = req.body.team.map(data => {
            if (data.role === "owner") {
              data.user_email = userData.email
            }
            return data
          })

          let workspaceData = await workspace.create(req.body)
          let planData = await UserFeatureSettings.findOne({ userId: workspaceData.user_id }).populate('planId')
          workspaceData = { ...workspaceData._doc, userPlanData: planData };

          // send email to team members
          workspaceData.team.map((data, index) => {
            if (data.user_email !== workspaceData.user_email) {
              sendEmail(workspaceData._id, req.body.team[index].user_email, workspaceData.workspace_name, req.body.user_name)
            }
          })

          return res.status(201).json({
            message: 'workspace added successfully...',
            data: workspaceData
          })
        } else {
          return res.status(400).json({
            error: "Upgrade to create more workspace"
          })
        }
      }
    } else {
      return res.status(400).json({
        error: 'bad request'
      })
    }
  }
  catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error
    })
  }
}

const deleteWorkspace = async (req, res) => {
  try {
    const data = await workspace.deleteOne({ _id: req.params.id, default_personal: false });

    if (data !== null) {
      // get the user default workspace and show to user after deleting workspace
      const defaultWorkspace = await workspace.find({ user_id: req.params.userId, default_personal: true })
      const userData = await user.findById(req.params.userId)

      if (userData !== null && defaultWorkspace.length !== 0) {
        userData.active_workspace = defaultWorkspace[0]._id
        userData.active_workspace_name = defaultWorkspace[0].workspace_name
        await userData.save()
      }

      return res.status(200).json({
        message: 'workspace delete successfully...',
        defaultWorkspace: defaultWorkspace[0]
      })
    } else {
      return res.status(404).json({
        error: 'No workspace found...'
      })
    }

  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const updateWorkspace = async (req, res) => {
  try {
    const workspaceData = await workspace.findOne({ _id: req.body.id })
    if (workspaceData !== null) {
      workspaceData.user_id = req.body.user_id;
      workspaceData.created_by_admin = req.body.created_by_admin
      workspaceData.workspace_name = req.body.workspace_name
      workspaceData.timeStamp = req.body.timeStamp
      workspaceData.workspace_description = req.body.workspace_description
      workspaceData.team = req.body.team
      workspaceData.total_team_member = req.body.total_team_member
      workspaceData.default_personal = req.body.default_personal
      workspaceData.save()
      return res.status(200).json({
        message: 'workspace updated successfully',
        data: workspaceData
      });

    } else {
      return res.status(401).json({
        error: 'workspace does not found...',
      });
    }

  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const viewWorkspace = async (req, res) => {
  try {
    const workspaceData = await workspace.findById(req.body.id)
    return res.status(200).json({
      message: 'workspace data fetch successfully',
      data: workspaceData
    });

  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }

}

const getAllWorkspaceAsPerUserID = async (req, res) => {
  try {
    const workspaceData = await workspace.find({ user_id: req.params.user_id })
    const groupsData = await workspace.find({ team: { $elemMatch: { user_email: req.params.email } }, active: true })
    let workspaceGroupData = workspaceData.concat(groupsData)
    let dups = [];

    let allworkspaceList = workspaceGroupData.filter(function (el) {
      // If it is not a duplicate, return true
      if (dups.indexOf(el.workspace_name) == -1) {
        dups.push(el.workspace_name);
        return true;
      }
      return false;
    })

    let allworkspaceList1 = []
    allworkspaceList.forEach(data => {
      data.team.forEach(data1 => {
        if (data1.active === true && data1.user_email === req.params.email) allworkspaceList1.push(data)
      })
    })

    return res.status(200).json({
      message: 'workspace data fetch successfully',
      data: allworkspaceList1,
    });
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const getAllworkspace = async (req, res) => {
  try {
    let data = await workspace.find({})

    for await (const data1 of data) {
      data1.active === true
      await data1.save()
    }

    res.send(data)
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const sendEmail = (id, email, workspaceName, adminEmail, name, teamMembers, userEmail) => {
  const mailOptions = {
    from: 'Creatosaurus <account@creatosaurus.io>',
    to: email,
    subject: `${name} has invited you to work with them in Creatosaurus 🚀" `,
    html: `
    <!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <title>

    </title>
    <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    <!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    <!--[if lte mso 11]>
        <style type="text/css">
          .outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->

    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Poppins:400,700);
    </style>
    <!--<![endif]-->



    <style type="text/css">
        @media only screen and (max-width:480px) {
            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100%;
            }
        }
    </style>


    <style type="text/css">
        @media only screen and (max-width:480px) {
            table.full-width-mobile {
                width: 100% !important;
            }

            td.full-width-mobile {
                width: auto !important;
            }
        }
    </style>
    <style type="text/css">
        .hide_on_mobile {
            display: none !important;
        }

        @media only screen and (min-width: 480px) {
            .hide_on_mobile {
                display: block !important;
            }
        }

        .hide_section_on_mobile {
            display: none !important;
        }

        @media only screen and (min-width: 480px) {
            .hide_section_on_mobile {
                display: table !important;
            }

            div.hide_section_on_mobile {
                display: block !important;
            }
        }

        .hide_on_desktop {
            display: block !important;
        }

        @media only screen and (min-width: 480px) {
            .hide_on_desktop {
                display: none !important;
            }
        }

        .hide_section_on_desktop {
            display: table !important;
            width: 100%;
        }

        @media only screen and (min-width: 480px) {
            .hide_section_on_desktop {
                display: none !important;
            }
        }

        p,
        h1,
        h2,
        h3 {
            margin: 0px;
        }

        ul,
        li,
        ol {
            font-size: 11px;
            font-family: Ubuntu, Helvetica, Arial;
        }

        a {
            text-decoration: none;
            color: inherit;
        }

        @media only screen and (max-width:480px) {

            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100% !important;
            }

            .mj-column-per-100>.mj-column-per-75 {
                width: 75% !important;
                max-width: 75% !important;
            }

            .mj-column-per-100>.mj-column-per-60 {
                width: 60% !important;
                max-width: 60% !important;
            }

            .mj-column-per-100>.mj-column-per-50 {
                width: 50% !important;
                max-width: 50% !important;
            }

            .mj-column-per-100>.mj-column-per-40 {
                width: 40% !important;
                max-width: 40% !important;
            }

            .mj-column-per-100>.mj-column-per-33 {
                width: 33.333333% !important;
                max-width: 33.333333% !important;
            }

            .mj-column-per-100>.mj-column-per-25 {
                width: 25% !important;
                max-width: 25% !important;
            }

            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100% !important;
            }

            .mj-column-per-75 {
                width: 100% !important;
                max-width: 100% !important;
            }

            .mj-column-per-60 {
                width: 100% !important;
                max-width: 100% !important;
            }

            .mj-column-per-50 {
                width: 100% !important;
                max-width: 100% !important;
            }

            .mj-column-per-40 {
                width: 100% !important;
                max-width: 100% !important;
            }

            .mj-column-per-33 {
                width: 100% !important;
                max-width: 100% !important;
            }

            .mj-column-per-25 {
                width: 100% !important;
                max-width: 100% !important;
            }
        }
    </style>

</head>

<body style="background-color:#F8F8F9;">

    <div
        style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
        Join your team in ${workspaceName} workspace on Creatosaurus
    </div>


    <div style="background-color:#F8F8F9;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
            style="background:#FFFFFF;background-color:#FFFFFF;width:100%;">
            <tbody>
                <tr>
                    <td>
                        <div style="margin:0px auto;max-width:600px;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                                style="width:100%;">
                                <tbody>
                                    <tr>
                                        <td
                                            style="direction:ltr;font-size:0px;padding:0px 0px 0px 0px;text-align:center;">
                                            <div class="mj-column-per-100 outlook-group-fix"
                                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                    style="vertical-align:top;" width="100%">

                                                    <tr>
                                                        <td style="font-size:0px;word-break:break-word;">
                                                            <div style="height:30px;">
                                                                &nbsp;
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="left"
                                                            style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">

                                                            <div
                                                                style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;">
                                                                <p
                                                                    style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">
                                                                    <strong>CREATOSAURUS</strong>
                                                                </p>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-size:0px;word-break:break-word;">
                                                            <div style="height:10px;">
                                                                &nbsp;
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="left"
                                                            style="font-size:0px;padding:15px 15px 5px 15px;word-break:break-word;">

                                                            <div
                                                                style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;">
                                                                <h1
                                                                    style="font-family: Poppins, sans-serif; font-size: 22px; text-align: center;">
                                                                    Join your team on Creatosaurus</h1>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="left"
                                                            style="font-size:0px;padding:5px 20px 5px 20px;word-break:break-word;">

                                                            <div
                                                                style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;">
                                                                <p
                                                                    style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">
                                                                    <span
                                                                        style="text-transform:capitalize">${name}</span>
                                                                    (${userEmail}) has invited you to use Creatosaurus
                                                                    with them, in a workspace called ${workspaceName}.
                                                                </p>
                                                            </div>

                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td align="center" vertical-align="middle"
                                                            style="font-size:0px;padding:20px 20px 5px 20px;word-break:break-word;">

                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                role="presentation"
                                                                style="border-collapse:separate;line-height:100%;">
                                                                <tr>
                                                                    <td align="center" bgcolor="#FF4359"
                                                                        role="presentation"
                                                                        style="border:none;border-radius:24px;cursor:auto;mso-padding-alt:9px 26px 9px 26px;background:#FF4359;"
                                                                        valign="middle">
                                                                        <a href="https://www.creatosaurus.io/invitation/${id}/${email}"
                                                                            style="display: inline-block; background: #FF4359; color: #ffffff; font-family: Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 100%; margin: 0; text-decoration: none; text-transform: none; padding: 9px 26px 9px 26px; mso-padding-alt: 0px; border-radius: 24px;"
                                                                            target="_blank">
                                                                            <span>View invitation</span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>


                                                    <tr>
                                                        <td style="font-size:0px;word-break:break-word;">
                                                            <div style="height:40px;">
                                                                &nbsp;
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
    `
  }

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      logger.error(error)
    }
  });
};

const ConfirmWorkspace = async (req, res) => {
  try {

    let workspaceData = await workspace.findById(req.params.id)

    workspaceData.team = workspaceData.team.map((data) => {
      if (data.user_email === req.params.email) {
        data.status = true
      }
      return data
    })

    await workspaceData.save()

    const userExist = await user.findOne({ email: req.params.email })

    if (userExist === null) {
      res.redirect('https://www.app.creatosaurus.io/signup')
    } else {
      res.redirect('https://www.app.creatosaurus.io')
    }

    if (workspaceData === null) return res.send("You haven't sign up yet please sign up")

  } catch (error) {
    logger.error(error)
    res.send("You haven't sign up yet please sign up")
  }
}

const capitalizeFirstLetter = (string) => {
  return string?.charAt(0).toUpperCase() + string?.slice(1);
}

const inviteTeamMembers = async (req, res) => {
  try {
    const userData = await user.findOne({ _id: req.body.userId })
    const planData = await UserFeatureSettings.findOne({ userId: req.body.userId }).populate('planId')
    const workspaceDataAll = await workspace.find({ user_id: req.body.userId })

    let firstName = capitalizeFirstLetter(userData.firstName)
    let lastName = capitalizeFirstLetter(userData.lastName)

    if (isNaN(firstName) && typeof firstName !== 'string') {
      firstName = ""
    }

    if (isNaN(lastName) && typeof lastName !== 'string') {
      lastName = ""
    }

    let name = firstName + " " + lastName
    let teamMembers = workspaceDataAll[0]?.team.length - 1

    let emails = []

    workspaceDataAll.forEach(data => {
      data.team.forEach(team => {
        emails.push(team.user_email)
      })
    })

    emails = [...new Set(emails)]

    if (planData.planId.editorSeats !== undefined) {
      let ownerCount = 1
      let editorCount = ownerCount

      // check user editor seats
      workspaceDataAll.forEach(data => {
        data.team.forEach(team => {
          if (team.role !== "owner" && team.role !== "view") {
            editorCount = editorCount + 1
          }
        })
      })

      //  validate user seats
      if (editorCount >= planData.planId.editorSeats && req.body.team.role !== "view") {
        return res.status(400).json({
          error: 'Your editor seats limit is reached. Upgrade to add more users.'
        })
      }
    }

    if (planData.users === null) {
      let workspaceData = await workspace.findOne({ _id: req.body.id })

      if (workspaceData === null) return res.status(400).json({
        error: 'Workspace not found'
      })

      let checkEmailIsAlreadyThere = false
      workspaceData.team.map(data => {
        if (data.user_email === req.body.team.user_email) {
          checkEmailIsAlreadyThere = true
        }
      })

      if (checkEmailIsAlreadyThere) return res.status(409).json({
        error: 'Workspace already contains this user',
      });

      workspaceData.team.push(req.body.team)
      const updatedWorkspaceData = await workspaceData.save()

      const updatedTeamData = await Promise.all(updatedWorkspaceData.team.map(async data => {
        let userData = await user.findOne({ email: data.user_email })
        return { ...data._doc, firstName: userData?.firstName }
      }))

      let planData = await UserFeatureSettings.findOne({ userId: workspaceData.user_id }).populate('planId')
      const updatedWorkspaceDataWithTeam = { ...updatedWorkspaceData._doc, team: updatedTeamData, planData }

      sendEmail(updatedWorkspaceDataWithTeam._id, req.body.team.user_email, updatedWorkspaceDataWithTeam.workspace_name, req.body.user_name, name, teamMembers, userData.email)

      return res.status(200).json({
        message: 'workspace updated successfully',
        data: updatedWorkspaceDataWithTeam
      });
    }

    if (emails.length < parseInt(planData.users)) {
      let workspaceData = await workspace.findOne({ _id: req.body.id })

      if (workspaceData !== null) {
        let checkEmailIsAlreadyThere = false
        workspaceData.team.map(data => {
          if (data.user_email === req.body.team.user_email) {
            checkEmailIsAlreadyThere = true
          }
        })

        if (checkEmailIsAlreadyThere) return res.status(409).json({
          error: 'User already added to workspace ...',
        });

        workspaceData.team.push(req.body.team)
        const updatedWorkspaceData = await workspaceData.save()

        const updatedTeamData = await Promise.all(updatedWorkspaceData.team.map(async data => {
          let userData = await user.findOne({ email: data.user_email })
          return { ...data._doc, firstName: userData?.firstName }
        }))

        let planData = await UserFeatureSettings.findOne({ userId: workspaceData.user_id }).populate('planId')
        const updatedWorkspaceDataWithTeam = { ...updatedWorkspaceData._doc, team: updatedTeamData, planData }

        sendEmail(updatedWorkspaceDataWithTeam._id, req.body.team.user_email, updatedWorkspaceDataWithTeam.workspace_name, req.body.user_name, name, teamMembers, userData.email)

        return res.status(200).json({
          message: 'workspace updated successfully',
          data: updatedWorkspaceDataWithTeam
        });
      } else {
        return res.status(401).json({
          error: 'workspace does not found...',
        });
      }
    } else {
      return res.status(400).json({
        error: 'Upgrade to add more users'
      })
    }
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const updateMemberPermission = async (req, res) => {
  try {
    const workspaceData = await workspace.findOne({ _id: req.body.id })
    if (workspaceData !== null) {
      const workspaceDataAll = await workspace.find({ user_id: workspaceData.user_id })
      const planData = await UserFeatureSettings.findOne({ userId: workspaceData.user_id }).populate('planId')

      let isUserOldRoleView = false
      workspaceData.team = workspaceData.team.map((data) => {
        if (data.user_email === req.body.email) {
          if (data.role === "view") {
            isUserOldRoleView = true
          }
        }
        return data
      })


      if (planData.planId.editorSeats !== undefined && isUserOldRoleView === true) {
        let ownerCount = 1
        let editorCount = ownerCount

        // check user editor seats
        workspaceDataAll.forEach(data => {
          data.team.forEach(team => {
            if (team.role !== "owner" && team.role !== "view") {
              editorCount = editorCount + 1
            }
          })
        })


        //  validate user seats
        if (editorCount >= planData.planId.editorSeats) {
          return res.status(400).json({
            error: 'Your editor seats limit is reached. Upgrade to add more users.'
          })
        }
      }

      workspaceData.team = workspaceData.team.map((data) => {
        if (data.user_email === req.body.email) {
          data.role = req.body.role
        }
        return data
      })
      workspaceData.save()
      return res.status(200).json({
        message: 'member permission updated successfully',
        data: workspaceData
      });
    }
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });
  }
}

const removeMemberFromWorkspace = async (req, res) => {
  try {
    const workspaceData = await workspace.findOne({ _id: req.body.id })
    if (workspaceData !== null) {
      workspaceData.team = workspaceData.team.filter(function (el) { return el.user_email !== req.body.email; });
      workspaceData.save()
      return res.status(200).json({
        message: 'remvoed member from workspace successfully',
        data: workspaceData
      });

    }
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error,
    });

  }
}

const updateWorkspaceName = async (req, res) => {
  try {
    const workspaceData = await workspace.findOne({ _id: req.body.id })
    if (workspaceData !== null) {
      const worksapcesOfUser = await workspace.find({ user_id: req.body.user_id })

      let checkUserHasSameNameOfWorkspaceAsPreviousWorkspaces = false
      worksapcesOfUser.forEach(data => {
        if (data.workspace_name.toLowerCase().trim() === req.body.workspace_name.toLowerCase().trim()) {
          checkUserHasSameNameOfWorkspaceAsPreviousWorkspaces = true
        }
      })

      if (checkUserHasSameNameOfWorkspaceAsPreviousWorkspaces) return res.status(409).json({
        error: "The workspace name has been duplicated, please try another workspace name."
      })

      workspaceData.workspace_name = req.body.workspace_name
      workspaceData.workspace_description = req.body.workspace_description
      workspaceData.save()
      return res.status(200).json({
        message: 'workspace name updated successfully',
        data: workspaceData
      });

    } else {
      return res.status(401).json({
        error: 'workspace does not found...',
      });
    }
  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error
    });

  }
}

const getUserTimeZone = async (req, res) => {
  try {
    const data = await workspace.find({ _id: { $in: req.body.workspaceIds } })

    if (data.length) {
      const userData = await user.find({ _id: { $in: data.map(workspaceData => workspaceData.user_id) } })
      if (userData.length) {
        return res.status(200).json({
          data: userData
        });
      } else {
        return res.status(200).json({
          data: null
        });
      }

    } else {
      return res.status(200).json({
        data: null
      });
    }



  } catch (error) {
    logger.error(error)
    return res.status(500).json({
      error: error
    });

  }
}

module.exports = {
  viewWorkspace,
  updateWorkspace,
  deleteWorkspace,
  CreateWorkspace,
  getAllWorkspaceAsPerUserID,
  getAllworkspace,
  ConfirmWorkspace,
  inviteTeamMembers,
  updateMemberPermission,
  removeMemberFromWorkspace,
  updateWorkspaceName,
  getUserTimeZone
}



