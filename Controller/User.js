const user = require('../Schema/User');
const afilliate = require('../Schema/Afilliate');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const jwt_decode = require("jwt-decode");
const keySecret = 'kv1IDb96qqZnc0w0AINeNkCxvbqJsdDLklu76eKpIjY';
const axios = require("axios")
const nodemailer = require('nodemailer');
const sesTransport = require('nodemailer-ses-transport');
const objectId = require('mongoose').Types.ObjectId;
const SubAfilliate = require("../Schema/SubAffiliate")
const transporter = nodemailer.createTransport(
  sesTransport({
    accessKeyId: 'AKIAQ5RO3HORQD7AMD4E',
    secretAccessKey: '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',
    region: 'ap-south-1',
    rateLimit: 5,
  })
);

const logger = require("../logger");
const waitlist = require('../Schema/Waitlist');
const Workspace = require('../Schema/Workspace');
const { createUserFeatureSettingsDefault } = require('../Functions/featureSettings');

const sendEmailToCreatosaurus = (id, email) => {
  const mailOptions = {
    from: 'Creatosaurus <account@creatosaurus.io>',
    to: "creatosaurus.in@gmail.com",
    subject: "New user signup in creatosaurus",
    text: `New user created with the email ${email} and id of the user is ${id}`,
  }

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      logger.error(error);
    }
  });
}

const sendEmail = (id, email) => {
  const mailOptions = {
    from: 'Creatosaurus <account@creatosaurus.io>',
    to: email,
    subject: 'Confirm Your Account',
    html: `
    <!doctype html>
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <title>
          
        </title>
        <!--[if !mso]><!-- -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
          #outlook a { padding:0; }
          body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
          table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
          img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
          p { display:block;margin:13px 0; }
        </style>
        <!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <!--[if lte mso 11]>
        <style type="text/css">
          .outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
        
      <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet" type="text/css">
        <style type="text/css">
          @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
@import url(https://fonts.googleapis.com/css?family=Poppins:400,700);
        </style>
      <!--<![endif]-->

    
        
    <style type="text/css">
      @media only screen and (max-width:480px) {
        .mj-column-per-100 { width:100% !important; max-width: 100%; }
      }
    </style>
    
  
        <style type="text/css">
        
        

    @media only screen and (max-width:480px) {
      table.full-width-mobile { width: 100% !important; }
      td.full-width-mobile { width: auto !important; }
    }
  
        </style>
        <style type="text/css">.hide_on_mobile { display: none !important;} 
        @media only screen and (min-width: 480px) { .hide_on_mobile { display: block !important;} }
        .hide_section_on_mobile { display: none !important;} 
        @media only screen and (min-width: 480px) { 
            .hide_section_on_mobile { 
                display: table !important;
            } 

            div.hide_section_on_mobile { 
                display: block !important;
            }
        }
        .hide_on_desktop { display: block !important;} 
        @media only screen and (min-width: 480px) { .hide_on_desktop { display: none !important;} }
        .hide_section_on_desktop { 
            display: table !important;
            width: 100%;
        } 
        @media only screen and (min-width: 480px) { .hide_section_on_desktop { display: none !important;} }
        
          p, h1, h2, h3 {
              margin: 0px;
          }

          ul, li, ol {
            font-size: 11px;
            font-family: Ubuntu, Helvetica, Arial;
          }

          a {
              text-decoration: none;
              color: inherit;
          }

          @media only screen and (max-width:480px) {

            .mj-column-per-100 { width:100%!important; max-width:100%!important; }
            .mj-column-per-100 > .mj-column-per-75 { width:75%!important; max-width:75%!important; }
            .mj-column-per-100 > .mj-column-per-60 { width:60%!important; max-width:60%!important; }
            .mj-column-per-100 > .mj-column-per-50 { width:50%!important; max-width:50%!important; }
            .mj-column-per-100 > .mj-column-per-40 { width:40%!important; max-width:40%!important; }
            .mj-column-per-100 > .mj-column-per-33 { width:33.333333%!important; max-width:33.333333%!important; }
            .mj-column-per-100 > .mj-column-per-25 { width:25%!important; max-width:25%!important; }

            .mj-column-per-100 { width:100%!important; max-width:100%!important; }
            .mj-column-per-75 { width:100%!important; max-width:100%!important; }
            .mj-column-per-60 { width:100%!important; max-width:100%!important; }
            .mj-column-per-50 { width:100%!important; max-width:100%!important; }
            .mj-column-per-40 { width:100%!important; max-width:100%!important; }
            .mj-column-per-33 { width:100%!important; max-width:100%!important; }
            .mj-column-per-25 { width:100%!important; max-width:100%!important; }
        }</style>
        
      </head>
      <body style="background-color:#F8F8F9;">
        
    <div style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
      Creatosaurus confirm email
    </div>
  
        
      <div style="background-color:#F8F8F9;">
        
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#FFFFFF;background-color:#FFFFFF;width:100%;">
        <tbody>
          <tr>
            <td>
              
        
      <!--[if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    
        
      <div style="margin:0px auto;max-width:600px;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td style="direction:ltr;font-size:0px;padding:0px 0px 0px 0px;text-align:center;">
                <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:600px;"
            >
          <![endif]-->
            
      <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="30" style="vertical-align:top;height:30px;">
      
    <![endif]-->
  
      <div style="height:30px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
            <tr>
              <td align="center" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;">
                
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
        <tbody>
          <tr>
            <td style="width:48px;">
              
        <a href="https://www.creatosaurus.io/" target="_blank" style="color: #0000EE;">
          
      <img alt="Creatosaurus" height="auto" src="https://s3-eu-west-1.amazonaws.com/topolio/uploads/634e334873887/1666069362.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="48">
    
        </a>
      
            </td>
          </tr>
        </tbody>
      </table>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><strong>CREATOSAURUS</strong></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="10" style="vertical-align:top;height:10px;">
      
    <![endif]-->
  
      <div style="height:10px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 5px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><h1 style="font-family: Poppins, sans-serif; font-size: 22px; text-align: center;">Please confirm your email</h1></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:5px 20px 5px 20px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="font-size: 13px; color: rgb(0, 0, 0);">We're excited to see you join Creatosaurus. We just need you to confirm your email address by clicking the button below👇🏻</span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="10" style="vertical-align:top;height:10px;">
      
    <![endif]-->
  
      <div style="height:10px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
            <tr>
              <td align="center" vertical-align="middle" style="font-size:0px;padding:10px 10px 10px 10px;word-break:break-word;">
                
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:auto;line-height:100%;">
        <tr>
          <td align="center" bgcolor="#FF4359" role="presentation" style="border:0px #000000 solid;border-radius:3px;cursor:auto;mso-padding-alt:10px 30px 10px 30px;background:#FF4359;" valign="middle">
            <a href="https://api.app.creatosaurus.io/creatosaurus/email/confirm/${id}" style="display: inline-block; background: #FF4359; color: #ffffff; font-family: Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 100%; margin: 0; text-decoration: none; text-transform: none; padding: 10px 30px 10px 30px; mso-padding-alt: 0px; border-radius: 3px;" target="_blank">
              <span><span style="font-family: Poppins, sans-serif;">Confirm Your Email</span></span>
            </a>
          </td>
        </tr>
      </table>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="20" style="vertical-align:top;height:20px;">
      
    <![endif]-->
  
      <div style="height:20px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.6;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="font-size: 13px;">or you can also copy and paste this URL into your browser</span><br><span><a style="font-size: 13px; color: #0000EE;" href="https://api.app.creatosaurus.io/creatosaurus/email/confirm/${id}" target="_blank" rel="noopener">https://api.app.creatosaurus.io/creatosaurus/email/confirm/${id}</a></span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:20px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94);">Any questions? Drop us a reply! </span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;padding:10px 10px;padding-top:10px;padding-right:10px;padding-bottom:10px;word-break:break-word;">
                
      <p style="font-family: Poppins, sans-serif; border-top: solid 1px #F5F5F5; font-size: 1; margin: 0px auto; width: 100%;">
      </p>
      
      <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #F5F5F5;font-size:1;margin:0px auto;width:580px;" role="presentation" width="580px"
        >
          <tr>
            <td style="height:0;line-height:0;">
              &nbsp;
            </td>
          </tr>
        </table>
      <![endif]-->
    
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">Follow us on</p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="center" style="font-size:0px;padding:5px 10px 5px 10px;word-break:break-word;">
                
      
     <!--[if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
      >
        <tr>
      
              <td>
            <![endif]-->
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                
      <tr>
        <td style="padding:4px;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
            <tr>
              <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                <a href="https://www.linkedin.com/company/creatosaurushq" target="_blank" style="color: #0000EE;">
                    <img alt="LinkedIn" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/linkedin.png" style="border-radius:3px;display:block;" width="20">
                  </a>
                </td>
              </tr>
          </table>
        </td>
        
      </tr>
    
              </table>
            <!--[if mso | IE]>
              </td>
            
              <td>
            <![endif]-->
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                
      <tr>
        <td style="padding:4px;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
            <tr>
              <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                <a href="https://twitter.com/home?status=https://twitter.com/creatosaurushq" target="_blank" style="color: #0000EE;">
                    <img alt="Twitter" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/twitter.png" style="border-radius:3px;display:block;" width="20">
                  </a>
                </td>
              </tr>
          </table>
        </td>
        
      </tr>
    
              </table>
            <!--[if mso | IE]>
              </td>
            
              <td>
            <![endif]-->
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                
      <tr>
        <td style="padding:4px;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
            <tr>
              <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                <a href="https://www.facebook.com/creatosaurushq" target="_blank" style="color: #0000EE;">
                    <img alt="Facebook" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/facebook.png" style="border-radius:3px;display:block;" width="20">
                  </a>
                </td>
              </tr>
          </table>
        </td>
        
      </tr>
    
              </table>
            <!--[if mso | IE]>
              </td>
            
              <td>
            <![endif]-->
              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                
      <tr>
        <td style="padding:4px;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:20px;">
            <tr>
              <td style="font-size:0;height:20px;vertical-align:middle;width:20px;">
                <a href="https://www.instagram.com/creatosaurus/" target="_blank" style="color: #0000EE;">
                    <img alt="Instagram" height="20" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/instagram.png" style="border-radius:3px;display:block;" width="20">
                  </a>
                </td>
              </tr>
          </table>
        </td>
        
      </tr>
    
              </table>
            <!--[if mso | IE]>
              </td>
            
          </tr>
        </table>
      <![endif]-->
    
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94);">Creatosaurus, 6th Floor, Ideas to Impacts Hub, Baner, Pune, India.<br><a href="https://www.creatosaurus.io/" target="_blank" rel="noopener" style="color: #0000EE;">creatosaurus.io</a></span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:5px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(52, 73, 94); background-color: rgb(255, 255, 255);"><a href="https://www.creatosaurus.io/blog" target="_blank" rel="noopener" style="color: rgb(126, 140, 141);">Our Blog</a>  |  <a href="https://www.creatosaurus.io/contact" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Contact</a>  |  <a href="https://www.creatosaurus.io/terms" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Terms</a>  |  <a href="https://www.creatosaurus.io/privacy" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Privacy</a>  |  <a href="https://www.app.creatosaurus.io/help target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Help Center</a>  |  <a href="https://www.creatosaurus.io/" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)">Creatosaurus Community</a><br></span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:5px 15px 5px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><a href="https://www.app.creatosaurus.io/settings" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)"><span style="text-decoration: underline; color: rgb(126, 140, 141);">Manage your email preference</span></a><span style="color: rgb(126, 140, 141);">   |   <a href="https://www.app.creatosaurus.io/unsubscribe/${id}" target="_blank" rel="noopener" style="color: rgb(126, 140, 141)"><span style="text-decoration: underline;">Unsubscribe</span></a></span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;"><span style="color: rgb(126, 140, 141); background-color: rgb(255, 255, 255);">© 2025 Creatosaurus. All rights reserved by Creatosaurus</span></p></div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                
      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.2;text-align:left;color:#000000;"><p style="font-size: 11px; font-family: Poppins, sans-serif; text-align: center;">Made with ❤️ for Creators</p></div>
    
              </td>
            </tr>
          
            <tr>
              <td style="font-size:0px;word-break:break-word;">
                
      
    <!--[if mso | IE]>
    
        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="40" style="vertical-align:top;height:40px;">
      
    <![endif]-->
  
      <div style="height:40px;">
        &nbsp;
      </div>
      
    <!--[if mso | IE]>
    
        </td></tr></table>
      
    <![endif]-->
  
    
              </td>
            </tr>
          
      </table>
    
      </div>
    
          <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        
      </div>
    
        
      <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
    
      
            </td>
          </tr>
        </tbody>
      </table>
    
      </div>
    
      </body>
    </html>`,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      logger.error(error);
    }
  });
};

const Signupuser = async (req, res) => {
  try {
    if (req.body.password !== null) {
      // generate the hash ans assign to the password in body
      const hash = await bcrypt.hash(req.body.password, 10);
      req.body.password = hash;
    }

    // check the user exist in the data base or not
    const userExist = await user.findOne({ email: req.body.email });
    if (userExist !== null) {
      return res.status(409).json({
        error: 'already registered',
      });
    }

    // create the new user in data base and generate the token
    const Userdata = await user.create(req.body);
    const name = Userdata?.firstName || ""

    const defaultWorkspace = {
      user_id: Userdata._id,
      user_email: req.body.email,
      created_by_admin: true,
      workspace_name: `${name} workspace`,
      timeStamp: new Date(),
      workspace_description: "",
      team: [{
        user_email: req.body.email,
        role: "owner",
        status: true,
        active: true
      }],
      total_team_member: true,
      default_personal: true,
      active: true
    }

    await Workspace.create(defaultWorkspace)

    let obj = {
      userId: Userdata._id,
      afilliateUserAdded: []
    };

    // create the user afilliate data 
    await afilliate.create(obj);

    // check if user come from afilliate link
    if (req.body.invitationCodeFromUser !== null) {
      let afilliateData = await afilliate.findOne({ userId: req.body.invitationCodeFromUser });

      if (afilliateData === null) {
        let obj = {
          userId: req.body.invitationCodeFromUser,
          afilliateUserAdded: []
        };

        afilliateData = await afilliate.create(obj);
      }

      afilliateData.afilliateUserAdded.push(Userdata._id);
      await afilliateData.save();
    }

    await createUserFeatureSettingsDefault(Userdata._id)
    sendEmail(Userdata._id, Userdata.email);
    sendEmailToCreatosaurus(Userdata._id, Userdata.email)

    return res.status(201).json({
      id: Userdata._id,
      email: Userdata.email,
      isNewUser: true
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    });
  }
};



const Loginuser = async (req, res) => {
  try {
    if (req.body.authType === "google") {
      let tokenData
      if (req.body.type === "oneTap") {
        tokenData = jwt_decode(req.body.token)
      } else {
        const res = await axios.get(`https://www.googleapis.com/oauth2/v3/userinfo?access_token=${req.body.token}`)
        tokenData = res.data
      }

      const data = await user.findOne({ email: tokenData.email });

      if (data === null) {
        const name = tokenData?.given_name || ""
        let userName = tokenData.email.trim();
        userName = userName.slice(0, -10);

        let body = {
          email: tokenData.email,
          userName: userName,
          emailVerified: true,
          authType: "google",
          timeZone: req.body.timeZone,
          isAffiliate: req.body.affiliate === undefined || req.body.affiliate === null ? false : true,
          firstName: name,
          lastName: tokenData?.family_name || "",
        }

        const Userdata = await user.create(body);

        const defaultWorkspace = {
          user_id: Userdata._id,
          user_email: tokenData.email,
          created_by_admin: true,
          workspace_name: `${name} workspace`,
          timeStamp: new Date(),
          workspace_description: "",
          team: [{
            user_email: tokenData.email,
            role: "owner",
            status: true,
            active: true
          }],
          total_team_member: true,
          default_personal: true,
          active: true
        }

        await Workspace.create(defaultWorkspace)

        if (Userdata.isAffiliate) {
          if (req.body.invitationCodeFromUser !== null && req.body.invitationCodeFromUser !== undefined && objectId.isValid(req.body.invitationCodeFromUser)) {
            const subAfilliateData = await SubAfilliate.findOne({ subAffiliate: Userdata._id })
            if (subAfilliateData === null) {
              const referenceUser = await user.findById(req.body.invitationCodeFromUser)
              if (referenceUser !== null) {
                if (referenceUser.isAffiliate) {
                  await SubAfilliate.create({
                    userId: req.body.invitationCodeFromUser,
                    subAffiliate: Userdata._id
                  })
                }
              }
            }
          }
        }

        let obj = {
          userId: Userdata._id,
          afilliateUserAdded: []
        };

        // create the user afilliate data 
        await afilliate.create(obj);

        // check if user come from afilliate link
        if (req.body.invitationCodeFromUser !== null) {
          let afilliateData = await afilliate.findOne({ userId: req.body.invitationCodeFromUser });

          if (afilliateData === null) {
            let obj = {
              userId: req.body.invitationCodeFromUser,
              afilliateUserAdded: []
            };

            afilliateData = await afilliate.create(obj);
          }

          let waitlistUserData = await waitlist.findOne({ userId: req.body.invitationCodeFromUser })
          if (waitlistUserData !== null) {
            waitlistUserData.userWaitListNumber = waitlistUserData.userWaitListNumber - 100
            await waitlistUserData.save()
          }

          afilliateData.afilliateUserAdded.push(Userdata._id);
          await afilliateData.save();
        }


        await createUserFeatureSettingsDefault(Userdata._id)
        sendEmailToCreatosaurus(Userdata._id, Userdata.email)

        // waitlist code
        let number = 0
        const waitListData = await waitlist.findOne({ userId: Userdata._id })

        if (waitListData === null) {
          const count = await waitlist.count()

          let obj = {
            userId: Userdata._id,
            userWaitListNumber: 31301 + count
          }

          await waitlist.create(obj)
          number = obj.userWaitListNumber
        } else {
          number = waitListData.userWaitListNumber
        }
        // waitlist end

        const token = jwt.sign(
          {
            waitListNumber: number,
            isAdmin: Userdata.isAdmin,
            isAffiliate: Userdata.isAffiliate,
            userName: Userdata.userName,
            timeZone: Userdata.timeZone,
            id: Userdata._id,
            onboarding_status: Userdata.onboarding_status
          },
          keySecret,
          { expiresIn: `30d` }
        );

        return res.status(200).json({
          message: 'authentication succesfull',
          token,
          isNewUser: true,
          active_workspace: Userdata.active_workspace,
          active_workspace_name: Userdata.active_workspace_name
        });
      } else {
        if (req.body.affiliate !== undefined && data.isAffiliate !== true) {
          if (req.body.invitationCodeFromUser !== null && req.body.invitationCodeFromUser !== undefined && objectId.isValid(req.body.invitationCodeFromUser)) {
            const subAfilliateData = await SubAfilliate.findOne({ subAffiliate: data._id })
            if (subAfilliateData === null) {
              const referenceUser = await user.findById(req.body.invitationCodeFromUser)
              if (referenceUser !== null) {
                if (referenceUser.isAffiliate) {
                  await SubAfilliate.create({
                    userId: req.body.invitationCodeFromUser,
                    subAffiliate: data._id
                  })
                }
              }
            }
          }

          data.isAffiliate = true
          await data.save()
        }

        // waitlist code
        let number = 0
        const waitListData = await waitlist.findOne({ userId: data._id })

        if (waitListData === null) {
          const count = await waitlist.count()

          let obj = {
            userId: data._id,
            userWaitListNumber: 32301 + count
          }

          await waitlist.create(obj)
          number = obj.userWaitListNumber
        } else {
          number = waitListData.userWaitListNumber
        }
        // waitlist code end

        // generate the token
        const token = jwt.sign(
          {
            waitListNumber: number,
            isAdmin: data.isAdmin,
            isAffiliate: data.isAffiliate,
            userName: data.userName,
            timeZone: data.timeZone,
            id: data._id,
            onboarding_status: data.onboarding_status,
          },
          keySecret,
          { expiresIn: `30d` }
        );

        return res.status(200).json({
          message: 'authentication succesfull',
          token,
          active_workspace: data.active_workspace,
          active_workspace_name: data.active_workspace_name
        });
      }
    } else {
      const data = await user.findOne({ email: req.body.email });

      if (data === null) {
        return res.status(404).json({
          error: 'Account does not exists.',
        });
      } else {
        if (data.emailVerified === false)
          return res
            .status(401)
            .json({ error: 'Your email address is not verified.' });

        if (data.failedLoginAttempts === 5) {
          return res.status(404).json({
            error: 'Login attempts exceed.',
          });
        } else {
          // compare the hash password and requested password
          const result = await bcrypt.compare(req.body.password, data.password);
          if (!result) {
            data.failedLoginAttempts = data.failedLoginAttempts + 1;
            await data.save();
            return res.status(404).json({
              error: 'Authentication failed, please check your email and password.',
            });
          } else {
            data.failedLoginAttempts = 0;
            await data.save();

            // waitlist code
            let number = 0
            const waitListData = await waitlist.findOne({ userId: data._id })

            if (waitListData === null) {
              const count = await waitlist.count()

              let obj = {
                userId: data._id,
                userWaitListNumber: 32301 + count
              }

              await waitlist.create(obj)
              number = obj.userWaitListNumber
            } else {
              number = waitListData.userWaitListNumber
            }
            // waitlist code end

            // generate the token
            const token = jwt.sign(
              {
                waitListNumber: number,
                isAdmin: data.isAdmin,
                isAffiliate: data.isAffiliate,
                userName: data.userName,
                timeZone: data.timeZone,
                id: data._id,
                onboarding_status: data.onboarding_status,
              },
              keySecret,
              { expiresIn: `30d` }
            );

            return res.status(200).json({
              message: 'authentication succesfull',
              token,
              active_workspace: data.active_workspace,
              active_workspace_name: data.active_workspace_name
            });
          }
        }
      }
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      error: error,
    });
  }
};


const updateTimezone = async (req, res) => {
  try {
    const data = await user.findOne({ _id: req.body.id });
    if (data === null) {
      return res.status(404).json({
        message: 'user not found',
      });
    } else {
      data.timeZone = req.body.timeZone;
      await data.save();
      return res.status(200).json({
        message: 'timezone updated successfully',
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    });
  }
};

const verifyUser = async (req, res) => {
  try {
    const data = await user.findOne({ _id: req.body.id });
    if (data === null) {
      return res.status(404).json({
        message: 'auth failed',
      });
    } else {
      const token = jwt.sign(
        {
          data: data.email,
          id: data._id,
        },
        keySecret,
        { expiresIn: '30d' }
      );

      return res.status(200).json({
        message: 'authentication succesfull',
        token,
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    });
  }
};

const getUsersTimeZone = async (req, res) => {
  try {
    const data = await user.findOne({ _id: req.params.id });
    if (data === null) {
      return res.status(404).json({
        message: 'user not found',
      });
    } else {
      return res.status(200).json({
        timeZone: data.timeZone,
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    });
  }
};

const getUsersEmail = async (req, res) => {
  try {
    const data = await user.findOne({ _id: req.params.id });
    if (data === null) {
      return res.status(404).json({
        message: 'user not found',
      });
    } else {
      return res.status(200).json({
        email: data.email,
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    });
  }
};


const updateUserDetails = async (req, res) => {
  try {
    const userData = await user.findOne({ _id: req.body.id });
    if (!userData) return res.status(404).json({ error: "User not found" })

    if (req.body.onBoarding) {
      const workspaceData = await Workspace.findOne({ user_id: userData._id, default_personal: true })
      workspaceData.workspace_name = `${req.body.firstName} workspace`
      await workspaceData.save()
    }

    userData.firstName = req.body.firstName;
    userData.lastName = req.body.lastName;
    userData.describleYourself = req.body.describleYourself;
    userData.find_out_about_creatosaurus = req.body.find_out_about_creatosaurus;
    userData.onboarding_status = true;
    userData.save();
    return res.status(200).json({
      message: 'updated successfully',
      data: userData
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    });
  }
};


const updateActiveWorkspace = async (req, res) => {
  try {
    const userData = await user.findOne({ _id: req.body.user_id });
    if (userData !== null) {
      userData.active_workspace = req.body.active_workspace_id;
      userData.active_workspace_name = req.body.workspace_name;
      userData.save();
      return res.status(200).json({
        message: 'updatd workspace id successfully...',
        data: userData
      });
    } else {
      return res.status(401).json({
        error: 'user id does not found...',
      });

    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    });
  }
};

const getUserProfileDetails = async (req, res) => {
  try {
    const userProfileData = await user.findById({ _id: req.body.id });

    if (userProfileData !== null) {
      return res.status(200).json({
        message: 'profile fetch successfully...',
        data: userProfileData
      });
    } else {
      return res.status(200).json({
        message: 'user data not found...'
      });
    }
  } catch (error) {
    console.log(error)
    logger.error(error);
  }
};

module.exports = {
  Signupuser,
  Loginuser,
  verifyUser,
  updateTimezone,
  getUsersTimeZone,
  getUsersEmail,
  updateUserDetails,
  updateActiveWorkspace,
  getUserProfileDetails
};
