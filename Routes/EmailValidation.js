const express = require('express')
const user = require('../Schema/User')
const router = express.Router()
const logger = require("../logger")

router.get('/confirm/:id', async (req, res) => {
  try {
    const userData = await user.findById(req.params.id)
    if (userData === null) return res.send("You haven't sign up yet please sign up")
    userData.emailVerified = true
    await userData.save()
    res.redirect('https://www.creatosaurus.io/login')
  } catch (error) {
    logger.error(error)
    res.send("You haven't sign up yet please sign up")
  }
})

module.exports = router