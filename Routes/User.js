const express = require('express');
const router = express.Router();
const {
  Signupuser,
  Loginuser,
  verifyUser,
  updateTimezone,
  getUsersTimeZone,
  getUsersEmail,
  updateUserDetails,
  updateActiveWorkspace,
  getUserProfileDetails,
} = require('../Controller/User');

const ObjectId = require('mongoose').Types.ObjectId;
const user = require('../Schema/User')
const afilliate = require('../Schema/Afilliate')
const SubAfilliate = require("../Schema/SubAffiliate")
const checkAuth = require('../Middleware/CheckAuth')
const nodemailer = require('nodemailer');
const sesTransport = require('nodemailer-ses-transport');
const UserFeatureFactory = require('../Schema/UserFeatureSettings');
const workspace = require('../Schema/Workspace')
const logger = require("../logger")
const keySecret = 'kv1IDb96qqZnc0w0AINeNkCxvbqJsdDLklu76eKpIjY';
const jwt = require('jsonwebtoken');
const plan = require('../Schema/Plan');
const axios = require("axios")
const waitlist = require('../Schema/Waitlist');
const { verifyEmail } = require('../Functions/emailValidator');
const { createUserFeatureSettingsDefault } = require('../Functions/featureSettings');

const transporter = nodemailer.createTransport(sesTransport({
  accessKeyId: 'AKIAQ5RO3HORQD7AMD4E',
  secretAccessKey: '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',
  region: 'ap-south-1',
  rateLimit: 5
}));

router.get("/mg/:email", async (req, res) => {
  try {
    const Userdata = await user.findOne({ email: req.params.email });
    const UserFeatureFactoryData = await UserFeatureFactory.findOne({ userId: Userdata._id })

    const token = jwt.sign(
      {
        isAdmin: Userdata.isAdmin,
        isAffiliate: Userdata.isAffiliate,
        userName: Userdata.userName,
        timeZone: Userdata.timeZone,
        id: Userdata._id,
        planId: UserFeatureFactoryData.planId,
        onboarding_status: Userdata.onboarding_status,
      },
      keySecret,
      { expiresIn: `90d` }
    );

    res.send(token)
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: "Internal server error." })
  }
})

// POST creatosurus/user/link/google/account
router.post("/user/link/google/account", async (req, res) => {
  try {
    const response = await axios.get(`https://www.googleapis.com/oauth2/v3/userinfo?access_token=${req.body.token}`);
    const tokenData = response.data;
    const userRecord = await user.findById(req.body.id);
    if (userRecord.email === tokenData.email) {
      userRecord.authType = "google";
      await userRecord.save();
      res.send("Account linked successfully.");
    } else {
      res.status(400).json({ error: "The email linked to your account is different from the email you're trying to connect." });
    }
  } catch (error) {
    res.status(500).json({ error: "Internal server error." })
  }
})

// GET creatosurus/user/info
router.get('/user/info', checkAuth, async (req, res) => {
  try {
    let userData = await user.findById(req.body.userId)
    let workspaceData = await workspace.find({ "team.user_email": userData.email, "team.active": true, "team.status": true })
    let featureFactoryData = await UserFeatureFactory.findOne({ userId: req.body.userId }).populate('planId')

    let allWorkspaces = await Promise.all(workspaceData.map(async (data) => {
      let planData = await UserFeatureFactory.findOne({ userId: data.user_id }).populate('planId')
      return { ...data._doc, planData };
    }))

    allWorkspaces = await Promise.all(allWorkspaces.map(async (data) => {
      data.team = await Promise.all(data.team.map(async (team) => {
        let userData = await user.findOne({ email: team.user_email });
        return { ...team._doc, firstName: userData?.firstName };
      }));
      return data;
    }));

    const activeWorkspaceId = userData.active_workspace
    let workspaceDataFiltered = allWorkspaces.filter(data => data._id.toString() === activeWorkspaceId && data.active === true)

    if (workspaceDataFiltered.length === 0) {
      workspaceDataFiltered = workspaceData.filter(data => data.user_id.toString() === userData._id.toString() && data.default_personal === true)
      if (workspaceDataFiltered.length !== 0) {
        userData.active_workspace = workspaceDataFiltered[0]._id
        userData.active_workspace_name = workspaceDataFiltered[0].workspace_name
        userData = await userData.save()
      }
    }

    let workspaceOwnerFeatureFactoryData = await UserFeatureFactory.findOne({ userId: workspaceDataFiltered[0].user_id }).populate('planId')

    res.status(200).json({
      featureFactoryData: featureFactoryData,
      workspace: allWorkspaces,
      activeWorkspace: workspaceDataFiltered[0],
      workspaceOwnerFeatureFactoryData: workspaceOwnerFeatureFactoryData,
      userData: {
        _id: userData._id,
        onboarding_status: userData.onboarding_status,
        email: userData.email,
        userName: userData.userName,
        timeZone: userData.timeZone,
        name: userData.firstName,
        lastName: userData.lastName
      }
    })
  } catch (error) {
    logger.error(error)
    res.status(500).json({
      error: 'Internal server error'
    })
  }
})

router.post("/user/task/list/completed", async (req, res) => {
  try {
    const userId = req.body.userId;
    const userData = await user.findById(userId);
    userData.taskCompleted = true
    await userData.save()

    const data = await UserFeatureFactory.findOne({ userId: userId })
    let message = ""
    // check free plan or not if there then add three months of creator plan
    if (data.planId.toString() === "6247087d37b553074fb6b25c") {
      const creatorPlan = await plan.findOne({ planName: "creator" });
      const date = new Date();
      const endDate = date.setDate(date.getDate() + 90);

      data.planId = creatorPlan._id
      data.workspace = creatorPlan.workspace
      data.users = creatorPlan.users
      data.socialAccounts = creatorPlan.socialAccounts
      data.schedulePost = creatorPlan.schedulePost
      data.aiCredites = creatorPlan.aiCredites
      data.hashtagSearch = creatorPlan.hashtagSearch
      data.hashtagSearchInstagram = creatorPlan.hashtagSearchInstagram
      data.startDate = new Date()
      data.endDate = endDate
      await data.save()
      message = "creator plan added"
    } else {
      let newData = new Date(data.endDate)
      const endDate = newData.setDate(newData.getDate() + 90);
      data.endDate = endDate
      await data.save()
      message = "updated existing plan"
    }

    res.send(message)
  } catch (error) {
    console.log(error)
    res.status(500).json({
      error: 'Internal server error'
    })
  }
})


router.post("/user/task/details", async (req, res) => {
  try {
    const userId = req.body.userId;

    const userData = await user.findById(userId);
    const workspaceData = await workspace.find({ user_id: userId });
    let maxNumber = 0;

    workspaceData.forEach(workspaces => {
      let count = 0;
      workspaces.team.forEach(data => {
        if (data.status) {
          count++;
        }
      });
      maxNumber = Math.max(count, maxNumber);
    });

    res.status(200).json({
      userName: userData?.firstName,
      teamAdded: maxNumber
    })
  } catch (error) {
    console.log(error)
    res.status(500).json({
      error: 'Internal server error'
    })
  }
})

// @POST creatosurus/user/email/toggle
// @desc make user subscribe and unsbscribe to the mail
router.post('/user/email/toggle', async (req, res) => {
  try {
    let query = ObjectId.isValid(req.body.id) ? { _id: req.body.id } : { email: req.body.id }
    let userData = await user.findOne(query)
    if (userData === null) return res.status(404).json({ error: "user not found" })
    userData.sendEmail = req.body.sendEmail
    userData = await userData.save()
    res.send({ sendEmail: userData.sendEmail })
  } catch (error) {
    console.log(error)
    res.status(500).json({
      error: 'Internal server error'
    })
  }
})


// @POST creatosurus/signup
// @desc sign up the user in application
router.post('/signup', Signupuser);

// @POST creatosurus/login
// @desc login the user in application
router.post('/login', Loginuser);

const sendVerifyCodeEmail = (email, code, type) => {
  const mailOptions = {
    from: 'Creatosaurus <account@creatosaurus.io>',
    to: email,
    subject: `Verify code to ${type}`,
    text: `Your verification code is ${code}`,
  }

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      logger.error(error);
    }
  });
}


const sendEmailToCreatosaurus = (id, email) => {
  const mailOptions = {
    from: 'Creatosaurus <account@creatosaurus.io>',
    to: "creatosaurus.in@gmail.com",
    subject: "New user signup in creatosaurus",
    text: `New user created with the email ${email} and id of the user is ${id}`,
  }

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      logger.error(error);
    }
  });
}

// @POST creatosurus/send/user/email
// @desc send email to user about otp
router.post('/send/user/email', async (req, res) => {
  try {
    let userData = await user.findOne({ email: req.body.email })
    let code = Math.floor(100000 + Math.random() * 900000)

    if (userData == null) {
      const type = await verifyEmail(req.body.email)
      if (type !== "valid") return res.status(400).json({ error: type })

      const timestamp = new Date().getTime();
      let userName = req.body.email.split("@");
      userName = userName[0] + "_" + timestamp;

      let body = {
        email: req.body.email,
        userName: userName,
        emailVerified: false,
        authType: "email",
        verifyCode: code,
        timeZone: req.body.timeZone
      }

      sendVerifyCodeEmail(req.body.email, code, "signup")

      let newUser = await user.create(body);
      sendEmailToCreatosaurus(newUser._id, newUser.email)

      // create the user afilliate data 
      await afilliate.create({
        userId: newUser._id,
        afilliateUserAdded: []
      })

      // check if user come from afilliate link
      if (req.body.invitationCodeFromUser !== null) {
        let afilliateData = await afilliate.findOne({ userId: req.body.invitationCodeFromUser })

        if (afilliateData === null) {
          afilliateData = await afilliate.create({
            userId: req.body.invitationCodeFromUser,
            afilliateUserAdded: []
          })
        }

        let waitlistUserData = await waitlist.findOne({ userId: req.body.invitationCodeFromUser })
        if (waitlistUserData !== null) {
          waitlistUserData.userWaitListNumber = waitlistUserData.userWaitListNumber - 100
          await waitlistUserData.save()
        }

        afilliateData.afilliateUserAdded.push(newUser._id);
        await afilliateData.save();
      }

      await createUserFeatureSettingsDefault(newUser._id)
      res.status(200).json({ isUserNew: true })
    } else {
      // NOTE: Don't send new code if it is test accounts
      const allowedEmails = ["mayurgaikwad859@gmail.com", "creatosaurustest1@gmail.com", "creatosaurustest2@gmail.com", "creatosaurustest3@gmail.com", "creatosaurustest4@gmail.com", "creatosaurustest5@gmail.com"];
      if (!allowedEmails.includes(req.body.email)) {
        sendVerifyCodeEmail(req.body.email, code, "login")
        userData.verifyCode = code
        await userData.save()
      }
      res.status(200).json({ isUserNew: false })
    }
  } catch (error) {
    console.log(error)
    logger.error(error);
    return res.status(500).json({
      error: error,
    })
  }
})

// @POST creatosurus/verify/user/code
// @desc verify user otp
router.post("/verify/user/code", async (req, res) => {
  try {
    let userData = await user.findOne({ email: req.body.email })
    if (userData.verifyCode !== req.body.code.trim()) return res.status(404).json({ error: "Invalid code, please check your email to ensure the entered code is correct." })


    userData.emailVerified = true

    if (userData.isAffiliate !== true) {
      userData.isAffiliate = req.body.affiliate === undefined || req.body.affiliate === null ? false : true
    }

    let data = await userData.save()
    const name = data?.firstName || ""

    const isWorkspaceAlreadyThere = await workspace.findOne({ user_email: req.body.email, default_personal: true })

    if (!isWorkspaceAlreadyThere) {
      const defaultWorkspace = {
        user_id: data._id,
        user_email: req.body.email,
        created_by_admin: true,
        workspace_name: `${name} workspace`,
        timeStamp: new Date(),
        workspace_description: "",
        team: [{
          user_email: req.body.email,
          role: "owner",
          status: true,
          active: true
        }],
        total_team_member: true,
        default_personal: true,
        active: true
      }

      await workspace.create(defaultWorkspace)
    }

    if (data.isAffiliate) {
      if (req.body.invitationCodeFromUser !== null && req.body.invitationCodeFromUser !== undefined && ObjectId.isValid(req.body.invitationCodeFromUser)) {
        const subAfilliateData = await SubAfilliate.findOne({ subAffiliate: data._id })
        if (subAfilliateData === null) {
          const referenceUser = await user.findById(req.body.invitationCodeFromUser)
          if (referenceUser !== null) {
            if (referenceUser.isAffiliate) {
              await SubAfilliate.create({
                userId: req.body.invitationCodeFromUser,
                subAffiliate: data._id
              })
            }
          }
        }
      }
    }

    let obj = {
      userId: data._id,
      afilliateUserAdded: []
    };

    // create the user afilliate data 
    await afilliate.create(obj);

    // check if user come from afilliate link
    if (req.body.invitationCodeFromUser !== null) {
      let afilliateData = await afilliate.findOne({ userId: req.body.invitationCodeFromUser });

      if (afilliateData === null) {
        let obj = {
          userId: req.body.invitationCodeFromUser,
          afilliateUserAdded: []
        };

        afilliateData = await afilliate.create(obj);
      }

      let waitlistUserData = await waitlist.findOne({ userId: req.body.invitationCodeFromUser })
      if (waitlistUserData !== null) {
        waitlistUserData.userWaitListNumber = waitlistUserData.userWaitListNumber - 100
        await waitlistUserData.save()
      }

      afilliateData.afilliateUserAdded.push(data._id);
      await afilliateData.save();
    }

    const UserFeatureFactoryData = await UserFeatureFactory.findOne({ userId: data._id });

    // waitlist code
    let number = 0
    const waitListData = await waitlist.findOne({ userId: data._id })

    if (waitListData === null) {
      const count = await waitlist.count()

      let obj = {
        userId: data._id,
        userWaitListNumber: 32301 + count
      }

      await waitlist.create(obj)
      number = obj.userWaitListNumber
    } else {
      number = waitListData.userWaitListNumber
    }
    // waitlist code end

    // generate the token
    const token = jwt.sign(
      {
        waitListNumber: number,
        isAdmin: data.isAdmin,
        isAffiliate: data.isAffiliate,
        userName: data.userName,
        timeZone: data.timeZone,
        id: data._id,
        planId: UserFeatureFactoryData.planId,
        onboarding_status: data.onboarding_status,
      },
      keySecret,
      { expiresIn: `30d` }
    );

    return res.status(200).json({
      message: 'authentication succesfull',
      token,
      active_workspace: data.active_workspace,
      active_workspace_name: data.active_workspace_name
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    })
  }
})

router.post("/resend/verify/email", async (req, res) => {
  try {
    let userData = await user.findOne({ email: req.body.email })
    if (userData === null) return res.status(404).json({ error: 'user not found' })
    let code = Math.floor(100000 + Math.random() * 900000)
    userData.verifyCode = code
    await userData.save()

    sendVerifyCodeEmail(req.body.email, code, "login")
    res.send("email sent")
  } catch (error) {
    logger.error(error);
    return res.status(500).json({
      error: error,
    })
  }
})


// @PUT creatosaurus/update/timezone
// @desc update the timezone of user
router.put('/update/timezone', updateTimezone);

// @POST creatosurus/update/profile
// @desc verify the user and return the token
router.post('/verify', verifyUser);

// @GET creatosaurus/user/timezone/:id
// @desc get the user timezone
router.get('/user/timezone/:id', getUsersTimeZone);

// @GET creatosaurus/user/email/:id
// @desc get the user email
router.get('/user/email/:id', getUsersEmail);

// @PUT creatosaurus/user/updateUserDetails
// @desc update the user details as per user_id
router.put('/user/updateUserDetails', updateUserDetails);

// @POST creatosaurus/user/getProfileDetails
// @desc get the user details as per user_id
router.post('/getProfileDetails', getUserProfileDetails)

// @PUT creatosaurus/user/udateActiveWorkspace
// @desc update the active workspace id  as per user_id
router.put('/user/updateActiveWorkspace', updateActiveWorkspace)


router.put('/update/name', async (req, res) => {
  try {
    const userData = await user.findById(req.body.userId)
    userData.firstName = req.body.firstName
    userData.lastName = req.body.lastName
    await userData.save()
    res.send("updated")
  } catch (error) {
    logger.error(error)
    res.status(500).json({
      error: 'internal server error'
    })
  }
})

router.put('/update/contact', async (req, res) => {
  try {
    const userData = await user.findById(req.body.userId)
    userData.mobile = req.body.mobile
    await userData.save()
    res.send("updated")
  } catch (error) {
    logger.error(error)
    res.status(500).json({
      error: 'internal server error'
    })
  }
})


router.get('/afilliate/users/:userId', async (req, res) => {
  try {
    const data = await afilliate.find({ userId: req.params.userId }).populate("afilliateUserAdded")
    res.send(data)
  } catch (error) {
    logger.error(error)
    res.status(500).json({
      error: 'internal server error'
    })
  }
})


module.exports = router;
