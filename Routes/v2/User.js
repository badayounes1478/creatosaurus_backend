const express = require('express');
const router = express.Router();
const checkAuth = require("../../Middleware/CheckAuth")
const user = require('../../Schema/User');
const workspace = require('../../Schema/Workspace');
const UserFeatureSettings = require('../../Schema/UserFeatureSettings');

router.get('/user/info', checkAuth, async (req, res) => {
    try {
        const userId = req.body.userId;

        // Fetch user data with specific fields excluded
        let userData = await user.findById(userId, {
            verifyCode: 0,
            userDetails: 0,
            failedLoginAttempts: 0,
            createdAt: 0,
            updatedAt: 0,
            malav2EmailSent: 0,
            malavEmailSent: 0,
            mayurEmailSent: 0,
            accountPaymentStatus: 0,
            active_workspace_name: 0,
            facebookId: 0,
            find_out_about_creatosaurus: 0,
            mobile: 0,
            password: 0,
            resetPasswordExpires: 0,
            resetPasswordToken: 0
        });

        if (!userData) {
            return res.status(404).json({ error: 'User not found' });
        }

        // Fetch the active workspace
        let activeWorkspace = await workspace.findOne({ _id: userData.active_workspace, "team.user_email": userData.email, "team.active": true, "team.status": true })

        // If active workspace is not found, fetch the default personal workspace
        if (!activeWorkspace) {
            activeWorkspace = await workspace.findOne(
                { user_id: userData._id, default_personal: true },
                { workspace_name: 1, _id: 1 }
            )

            if (!activeWorkspace) return res.status(404).json({ error: 'workspace not found' })

            userData.active_workspace = activeWorkspace._id
            userData.active_workspace_name = activeWorkspace.workspace_name
            userData = await userData.save()
        }

        const workspaceOwnerPlanData = await UserFeatureSettings.findOne({ userId: activeWorkspace.user_id }).populate('planId')

        res.status(200).json({
            userData,
            workspaceOwnerPlanData,
            activeWorkspace
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        })
    }
})


router.put('/user/updateActiveWorkspace', checkAuth, async (req, res) => {
    try {
        const { userId, active_workspace_id, workspace_name } = req.body;

        const userRecord = await user.findOne({ _id: userId });
        if (!userRecord) return res.status(404).json({ error: 'user not found' })

        userRecord.active_workspace = active_workspace_id;
        userRecord.active_workspace_name = workspace_name;
        await userRecord.save();

        return res.status(200).json({
            message: 'workspace updated'
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        })
    }
})

// creatosaurus/v2/user/info/update
router.put("/user/info/update", checkAuth, async (req, res) => {
    try {
        const userInfo = await user.findById(req.body.id)
        if (!userInfo) return res.status(404).json({ error: "User not found" })

        userInfo.firstName = req.body.firstName
        userInfo.lastName = req.body.lastName
        await userInfo.save()
        return res.status(200).json({ data: "User info updated" })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        })
    }
})

router.put("/user/timezone/update", checkAuth, async (req, res) => {
    try {
        const userInfo = await user.findById(req.body.id)
        if (!userInfo) return res.status(404).json({ error: "User not found" })

        userInfo.timeZone = req.body.timeZone
        await userInfo.save()
        return res.status(200).json({ data: "User info updated" })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        })
    }
})

router.get("/user/plan", checkAuth, async (req, res) => {
    try {
        const data = await UserFeatureSettings.findOne({ userId: req.body.userId }).populate("planId")
        res.send(data)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        })
    }
})

module.exports = router;