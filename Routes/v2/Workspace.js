const express = require('express');
const router = express.Router();
const checkAuth = require("../../Middleware/CheckAuth")
const user = require('../../Schema/User');
const workspace = require('../../Schema/Workspace');
const UserFeatureSettings = require('../../Schema/UserFeatureSettings');

router.get('/', checkAuth, async (req, res) => {
    try {
        const { userId } = req.body;

        let userData = await user.findById(userId, { _id: 1, email: 1, active_workspace: 1 });
        let userWorkspaces = await workspace.find({
            team: {
                $elemMatch: {
                    user_email: userData.email,
                    active: true,
                    status: true
                }
            }
        });

        const workspaceAddUserPlanData = userWorkspaces.map(async (workspace) => {
            try {
                const userPlanData = await UserFeatureSettings.findOne({ userId: workspace.user_id }).populate('planId');
                return {
                    ...workspace._doc,
                    userPlanData
                };
            } catch (error) {
                return {
                    ...workspace._doc,
                    userPlanData: null
                };
            }
        });

        userWorkspaces = await Promise.all(workspaceAddUserPlanData);
        const activeWorkspace = userWorkspaces.find(data => data._id.toString() === userData.active_workspace)

        res.status(200).json({
            data: userWorkspaces,
            activeWorkspace,
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: 'Internal server error'
        })
    }
})


/**
 * Fetch workspace invitation details.
 */
router.get('/invitation/details/:id/:email', async (req, res) => {
    const { id, email } = req.params;

    try {
        // Fetch workspace information
        const workspaceInfo = await workspace.findById(id);
        if (!workspaceInfo) {
            return res.status(404).json({ error: 'Workspace not found' });
        }

        // Check if the user is invited and the invitation is pending
        const invitedUser = workspaceInfo.team.find(member => member.user_email === email);
        if (!invitedUser) {
            return res.status(404).json({ error: 'User not found in workspace' });
        }

        // Fetch user and workspace owner information in parallel
        const [userInfo, workspaceOwnerInfo] = await Promise.all([
            user.findOne({ email }, 'userName firstName lastName email'),
            user.findById(workspaceInfo.user_id, 'userName firstName lastName email')
        ]);

        return res.status(200).json({ workspaceInfo, workspaceOwnerInfo, userInfo });
    } catch (error) {
        console.error('Error fetching invitation details:', error);
        return res.status(500).json({ error: 'An unexpected error occurred' });
    }
});


/**
 * Accept workspace invitation.
 */
router.post('/invitation/accept', checkAuth, async (req, res) => {
    const { id, email } = req.body;

    try {
        // Fetch workspace information
        const workspaceInfo = await workspace.findById(id);
        if (!workspaceInfo) {
            return res.status(404).json({ error: 'Workspace not found' });
        }

        // Update the user's invitation status
        const teamUpdated = workspaceInfo.team.map(member => {
            if (member.user_email === email) {
                return { ...member, status: true, denied: false };
            }
            return member;
        });

        workspaceInfo.team = teamUpdated;
        await workspaceInfo.save();

        return res.status(200).json({ message: 'User successfully added to the workspace' });
    } catch (error) {
        console.error('Error accepting invitation:', error);
        return res.status(500).json({ error: 'An unexpected error occurred' });
    }
});

/**
 * Cancel workspace invitation.
 */
router.post('/invitation/cancel', checkAuth, async (req, res) => {
    const { id, email } = req.body;

    try {
        // Fetch workspace information
        const workspaceInfo = await workspace.findById(id);
        if (!workspaceInfo) {
            return res.status(404).json({ error: 'Workspace not found' });
        }

        // Mark the invitation as denied
        const teamUpdated = workspaceInfo.team.map(member => {
            if (member.user_email === email) {
                return { ...member, denied: true };
            }
            return member;
        });

        workspaceInfo.team = teamUpdated;
        await workspaceInfo.save();

        return res.status(200).json({ message: 'User invitation successfully denied' });
    } catch (error) {
        console.error('Error canceling invitation:', error);
        return res.status(500).json({ error: 'An unexpected error occurred' });
    }
});


module.exports = router;