const express = require('express');
const router = express.Router();
const CheckAuth = require('../../Middleware/CheckAuth')
const AppSumo = require("../../Schema/AppSumo")
const LinkClicked = require("../../Schema/LinkClicked");
const Afilliate = require('../../Schema/Afilliate');
const Newsletter = require('../../Schema/Newsletter');
const Order = require('../../Schema/Order');
const PaymentAffiliate = require('../../Schema/PaymentAffiliate');
const SubAffiliate = require('../../Schema/SubAffiliate');
const User = require('../../Schema/User');
const Workspace = require('../../Schema/Workspace');
const UserFeatureSettings = require('../../Schema/UserFeatureSettings');

router.delete("/user/account", CheckAuth, async (req, res) => {
    try {
        const { userId } = req.body
        const userDetails = await User.findOne({ _id: userId })
        if (!userDetails) return res.status(404).json({ error: "User not found" })

        await Promise.all([
            Afilliate.deleteMany({ userId }),
            AppSumo.deleteMany({ userId }),
            LinkClicked.deleteMany({ userId }),
            Newsletter.deleteMany({ email: userDetails.email }),
            Order.deleteMany({ userId }),
            PaymentAffiliate.deleteMany({ userId }),
            SubAffiliate.deleteMany({ userId }),
            UserFeatureSettings.deleteMany({ userId }),
            Workspace.deleteMany({ user_id: userId }),
            User.deleteOne({ _id: userId })
        ]);

        res.send("account deleted")
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: "Internal server error" })
    }
})

module.exports = router;