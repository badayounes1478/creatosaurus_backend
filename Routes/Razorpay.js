const express = require("express")
const crypto = require("crypto")
const { createReazorPayInstance } = require("../config/razorpay.config.js")
const router = express.Router()
const razorpay = createReazorPayInstance()
const checkAuth = require('../Middleware/CheckAuth')
const payment = require("../Schema/Payment.js")
const plan = require("../Schema/Plan.js")
const order = require("../Schema/Order.js")
const couponCode = require("../Schema/CouponCode.js")
const constant = require("../utils/constant.js");
const axios = require("axios")
const { upgradeToPaidPlan } = require("../Functions/featureSettings.js")


// Utility function to generate invoice number
const generateInvoiceNumber = (count) => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth(); // January is 0, so March is 2

    let financialYear;
    if (currentMonth >= 2) {
        financialYear = `${currentYear.toString().slice(-2)}${(currentYear + 1).toString().slice(-2)}`;
    } else {
        financialYear = `${(currentYear - 1).toString().slice(-2)}${currentYear.toString().slice(-2)}`;
    }

    const number = count + 1
    const invoiceNumber = `C-${financialYear}-${number}`;
    return invoiceNumber;
}

router.post("/payment", checkAuth, async (req, res) => {
    // Fetch plan details
    const planDetails = await plan.findOne({ _id: req.body.id });
    if (!planDetails) return res.status(404).json({ error: "Plan not found" });

    // Fetch coupon data if provided
    let discount = null;
    if (req.body.code) {
        const couponCodeData = await couponCode.findOne({ _id: req.body.code });
        if (!couponCodeData) return res.status(404).json({ error: "Coupon code not found" });
        discount = couponCodeData.discount;
    }

    // Calculate the price based on duration and currency
    const priceUSD = req.body.duration === "monthly" ? planDetails.monthlyPriceUSD : planDetails.yearlyPriceUSD;
    const priceINR = req.body.duration === "monthly" ? planDetails.monthlyPriceINR : planDetails.yearlyPriceINR;
    let price = req.body.national ? priceINR : priceUSD;

    // calculate discount
    if (discount) {
        let discountAmount = (price * discount) / 100;
        price = price - discountAmount;
    }

    const currency = req.body.national ? "INR" : "USD"


    const options = {
        amount: price * 100,
        currency: currency,
        receipt: `receipt_${Date.now()}`
    }

    try {
        const orderData = await razorpay.orders.create(options)

        const data = {
            userId: req.body.userId,
            couponCode: req.body.code,
            currency: currency,
            userDetails: { ...req.body.userDetails },
            companyDetails: { ...req.body.companyDetails },
            planInfo: {
                planId: req.body.id,
                duration: req.body.duration,
            },
            orderDetails: { ...orderData }
        }

        await order.create(data)
        res.send(orderData)
    } catch (error) {
        console.error(error.response.data);
        return res.status(500).json({ error: "Internal server error" });
    }
})

const getPaymentInformation = async (paymentObject, paymentId) => {
    try {
        const response = await axios.get(`https://api.razorpay.com/v1/payments/${paymentId}`, {
            auth: {
                username: constant.rzp_key_id,
                password: constant.rzp_key_secret
            }
        })

        // Extract payment method from response data
        const { method } = response.data;

        paymentObject.paymentMethod = method
        await paymentObject.save()
    } catch (error) {
        console.log("failed to get payment method")
    }
}

router.post("/verify", checkAuth, async (req, res) => {
    try {
        const { order_id, payment_id, signature } = req.body
        const secretKey = constant.rzp_key_secret

        // create hmac object
        const hmac = crypto.createHmac("sha256", secretKey)
        hmac.update(order_id + "|" + payment_id)
        const generatedSignature = hmac.digest("hex")

        // Verify if signatures match
        if (generatedSignature !== signature) {
            return res.status(400).json({ error: "Payment verification failed. Invalid signature." });
        }

        const orderData = await order.findOne({ "orderDetails.id": order_id })
        if (!orderData) return res.status(404).json({ error: "Order not found" })

        const planDetails = await plan.findOne({ _id: orderData.planInfo.planId })

        // Get discount information if available
        let discount = null;
        if (orderData.couponCode) {
            const couponCodeData = await couponCode.findOne({ _id: orderData.couponCode });
            if (!couponCodeData) return res.status(404).json({ error: "Coupon code not found" });
            discount = couponCodeData.discount;
        }

        // Calculate the price based on duration and currency
        const priceUSD = orderData.planInfo.duration === "monthly" ? planDetails.monthlyPriceUSD : planDetails.yearlyPriceUSD;
        const priceINR = orderData.planInfo.duration === "monthly" ? planDetails.monthlyPriceINR : planDetails.yearlyPriceINR;
        const currency = orderData.currency
        let price = currency === "INR" ? priceINR : priceUSD;

        const originalAmount = price

        // calculate discount
        if (discount) {
            let discountAmount = (price * discount) / 100;
            price = price - discountAmount;
        }

        const finalAmount = price

        // Subscription dates logic
        const subscriptionStartDate = new Date(); //
        const subscriptionEndDate = new Date(subscriptionStartDate);
        if (orderData.planInfo.duration === "monthly") {
            subscriptionEndDate.setMonth(subscriptionEndDate.getMonth() + 1); // Add one month
        } else { // yearly
            subscriptionEndDate.setFullYear(subscriptionEndDate.getFullYear() + 1); // Add one year
        }

        const count = await payment.countDocuments({})
        const invoiceId = generateInvoiceNumber(count); // Generate unique invoice number

        const paymentDetails = {
            userId: req.body.userId,
            planId: orderData.planInfo.planId,
            invoiceId: invoiceId,
            paymentProvider: "razorpay",
            paymentMethod: "",
            paymentStatus: 'completed',
            originalAmount: originalAmount,
            finalAmount: finalAmount,
            currency: orderData.currency,
            discountDetails: {
                discountPercentage: discount,
                couponCode: orderData.couponCode,
            },
            subscriptionDetails: {
                subscriptionType: orderData.planInfo.duration,
                subscriptionStartDate: subscriptionStartDate,
                subscriptionEndDate: subscriptionEndDate,
                autoRenew: false
            },
            paymentMetaData: {
                paymentId: payment_id,
                ...orderData.orderDetails
            },
            userMetaData: {
                userDetails: { ...orderData.userDetails },
                companyDetails: { ...orderData.companyDetails }
            }
        }

        upgradeToPaidPlan(req.body.userId, orderData.planInfo.planId, orderData.planInfo.duration)
        const paymentObject = await payment.create(paymentDetails)
        getPaymentInformation(paymentObject, payment_id)
        return res.status(200).json({
            message: "Payment verified"
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            error: "Internal server error"
        })
    }
})

module.exports = router