const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const workspace = new Schema({
    user_id: String,
    user_email: String,
    created_by_admin: String,
    workspace_name: {
        type: String,
        lowercase: true
    },
    timeStamp: Date,
    workspace_description: {
        type: String,
        lowercase: true
    },
    team: [{
        user_email: {
            type: String,
            lowercase: true
        },
        role: String,
        status: Boolean,
        denied: Boolean,
        active: { type: Boolean, default: true }
    }]
    ,
    total_team_member: Number,
    default_personal: Boolean,
    active: { type: Boolean, default: true }
})

module.exports = mongoose.model('Workspace', workspace);