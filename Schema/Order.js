const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Order = new Schema({
    userId: { type: Schema.Types.ObjectId, required: true },
    couponCode: { type: String, default: '' },
    currency: { type: String, required: true },

    planInfo: {
        planId: { type: Schema.Types.ObjectId, required: true },
        duration: { type: String, required: true, enum: ['monthly', 'yearly', 'weekly', 'one-time'] },
    },

    userDetails: {
        email: { type: String, default: '' },
        firstName: { type: String, default: '' },
        lastName: { type: String, default: '' },
        postalCode: { type: String, default: '' },
        country: { type: String, default: '' },
        state: { type: String, default: '' },
        phoneNumber: { type: String, default: '' },
        address: { type: String, default: '' }
    },

    companyDetails: {
        name: { type: String, default: '' },
        postalCode: { type: String, default: '' },
        country: { type: String, default: '' },
        state: { type: String, default: '' },
        address: { type: String, default: '' },
        gst: { type: String, default: '' },
    },

    orderDetails: {
        type: Schema.Types.Mixed
    }
});

module.exports = mongoose.model('Order', Order);
