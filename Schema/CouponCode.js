const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const couponCode = new Schema({
    code: {
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
    name: String,
    used: Boolean,
    discount: Number,
    expireDate: Date
});

module.exports = mongoose.model('couponCode', couponCode);