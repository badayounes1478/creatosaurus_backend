const mongoose = require('mongoose');
const { Schema } = mongoose;

const appSumoSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    appSumoUserId: {
        type: String,
        required: true,
        unique: true,
    },
    planId: {
        type: String,
        required: true,
    },
    activationEmail: {
        type: String,
        required: true,
    },
    invoiceId: {
        type: String,
        required: true,
    },
}, {
    timestamps: true,
});

module.exports = mongoose.model('AppSumo', appSumoSchema);
