const mongoose = require('mongoose');
const schema = mongoose.Schema;

const SubAfilliate = new schema({
    userId: { type: schema.Types.ObjectId, ref: 'User' },
    subAffiliate: { type: schema.Types.ObjectId, ref: 'User' }
}, {
    timestamps: true
})

module.exports = mongoose.model('SubAfilliate', SubAfilliate);
