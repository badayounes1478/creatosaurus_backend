const mongoose = require('mongoose');
const { Schema } = mongoose;

const linkClickedSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    days: [{
        type: Date,
        required: true
    }]
})

module.exports = mongoose.model('LinkClicked', linkClickedSchema);
