const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let payment = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    planId: {
        type: Schema.Types.ObjectId,
        ref: 'Plan',
        required: true
    },
    invoiceId: {
        type: String,
        unique: true,
    },
    paymentProvider: {
        type: String,
        enum: ['stripe', 'razorpay', 'payPal', 'other'],
        required: true
    },
    paymentMethod: String,
    paymentStatus: {
        type: String,
        enum: ['pending', 'completed', 'failed', 'refunded'],
        default: 'pending',
        required: true
    },
    originalAmount: Number,
    finalAmount: Number,
    currency: String,
    discountDetails: {
        discountPercentage: Number,
        couponCode: String,
    },
    subscriptionDetails: {
        subscriptionType: {
            type: String,
            enum: ['monthly', 'yearly', 'weekly', 'one-time']
        },
        subscriptionStartDate: Date,
        subscriptionEndDate: Date,
        autoRenew: {
            type: Boolean,
            default: false
        }
    },
    paymentMetaData: {
        type: Schema.Types.Mixed
    },
    userMetaData: {
        type: Schema.Types.Mixed
    }
}, { timestamps: true });

module.exports = mongoose.model('Payment', payment);